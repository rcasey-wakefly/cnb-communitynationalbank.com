﻿var AccordionCtrl = (function ($) {

    function faqs() {

        var toggle = $('.pane-toggler-down'),
            toggles = $('.item-page').find(toggle),
            panes = $('.item-page').find('.pane-down');

        toggle.addClass('plus-icon');

        toggle.on('click', function () {

            var isOpen = $(this).hasClass('acc-down');

            if (!isOpen) {
                panes.slideUp();
                toggles.removeClass('acc-down');
                $(this).addClass('acc-down');
                $('.acc-down').next('.pane-down').slideToggle();
            } else {
                $('.acc-down').next('.pane-down').slideToggle();
                $(this).removeClass('acc-down');
            }

        });

    }

    $(document).ready(faqs);

})(jQuery);