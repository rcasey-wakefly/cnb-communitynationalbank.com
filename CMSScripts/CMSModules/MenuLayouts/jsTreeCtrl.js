﻿var JsTreeCtrl = (function ($) {
    var MEGA_MENU_GET_NODES_URL = '/cmsmodules/menulayouts/pages/megamenulayout.aspx/GetTreeNodes',
        MEGA_MENU_REMOVE_NODE_URL = '/cmsmodules/menulayouts/pages/megamenulayout.aspx/RemoveItem',
        MEGA_MENU_MOVE_NODE_URL = '/cmsmodules/menulayouts/pages/megamenulayout.aspx/MoveItem',

        FOOTER_MENU_GET_NODES_URL = '/cmsmodules/menulayouts/pages/footerlayout.aspx/GetTreeNodes',
        FOOTER_MENU_REMOVE_NODE_URL = '/cmsmodules/menulayouts/pages/footerlayout.aspx/RemoveItem',
        FOOTER_MENU_MOVE_NODE_URL = '/cmsmodules/menulayouts/pages/footerlayout.aspx/MoveItem',

        UTILITY_MENU_GET_NODES_URL = '/cmsmodules/menulayouts/pages/utilnavlayout.aspx/GetTreeNodes',
        UTILITY_MENU_REMOVE_NODE_URL = '/cmsmodules/menulayouts/pages/utilnavlayout.aspx/RemoveItem',
        UTILITY_MENU_MOVE_NODE_URL = '/cmsmodules/menulayouts/pages/utilnavlayout.aspx/MoveItem'

    var updateNodes = function (url, menuParams, inst) {
        var oldColumnIsEmpty = inst.get_container().find("li").length === 0;
        menuParams.IsOldColumnEmpty = oldColumnIsEmpty;

        if (oldColumnIsEmpty) {
            var instParent = inst.element.parent('.js-tree-wrapper');
            instParent.remove();
            inst.destroy();
        }

        $.ajax({
            type: 'POST',
            url: url,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify({layoutParams: menuParams})
        });
    };

    var removeItem = function (jsTreeInst, item, removeUrl) {
        var nodeGuid = item.li_attr.nodeGuid;
        var parent = item.parent;
        var oldColumn = jsTreeInst.element.attr('data-column');

        var parentNode = jsTreeInst.get_node(parent);
        var oldPosition = $.inArray(item.id, parentNode.children) + 1;

        var isOldColumnEmpty = false;

        var menuParams = {
            NodeGuid: nodeGuid,
            OldParent: parent,
            NewParent: parent,
            OldPosition: oldPosition,
            NewPosition: oldPosition,
            NewColumn: oldColumn,
            OldColumn: oldColumn,
            IsOldColumnEmpty: isOldColumnEmpty
        };

        if (jsTreeInst.is_selected(item)) {
            jsTreeInst.delete_node(jsTreeInst.get_selected());
        }
        else {
            jsTreeInst.delete_node(item);
        }

        setTimeout(function () { updateNodes(removeUrl, menuParams, jsTreeInst) }, 0);
    };

    var moveItem = function (data, moveUrl) {
        var oldInst = data.old_instance,
            newInst = data.new_instance;

        var oldParentNode = oldInst.get_node(data.old_parent);
        var newParentNode = newInst.get_node(data.parent);

        var nodeGuid = data.node.li_attr.nodeGuid,
        // jsTree will change the id of the li element when it's dragged into a new column, 
        // so use the custom nodeGuid attribute to guarantee we get the guid for the parent.
        // if the li_attr object is null than the parent is the root '#'
        oldParent = oldParentNode.li_attr ? oldParentNode.li_attr.nodeGuid : '#',
        newParent = newParentNode.li_attr ? newParentNode.li_attr.nodeGuid : '#',
        oldPosition = data.old_position,
        newPosition = data.position,
        newColumn = data.is_multi ? data.new_instance.element.attr('data-column') : data.instance.element.attr('data-column'),
        oldColumn = data.is_multi ? data.old_instance.element.attr('data-column') : data.instance.element.attr('data-column'),
        isOldColumnEmpty = false;

        var menuParams = {
            NodeGuid: nodeGuid,
            OldParent: oldParent,
            NewParent: newParent,
            OldPosition: oldPosition,
            NewPosition: newPosition,
            NewColumn: newColumn,
            OldColumn: oldColumn,
            IsOldColumnEmpty: isOldColumnEmpty
        };

        setTimeout(function () { updateNodes(moveUrl, menuParams, data.old_instance) }, 0);
    };

    var initTree = function (elem) {

        var type = elem.attr("data-type");
        var column = elem.attr("data-column");

        var url = '';
        var removeUrl = '';
        var moveUrl = '';

        switch (type.toLowerCase()) {
            case "megamenu":
                url = MEGA_MENU_GET_NODES_URL;
                removeUrl = MEGA_MENU_REMOVE_NODE_URL;
                moveUrl = MEGA_MENU_MOVE_NODE_URL;
                break;
            case "footer":
                url = FOOTER_MENU_GET_NODES_URL;
                removeUrl = FOOTER_MENU_REMOVE_NODE_URL;
                moveUrl = FOOTER_MENU_MOVE_NODE_URL;
                break;
            case "utilitynavigation":
                url = UTILITY_MENU_GET_NODES_URL;
                removeUrl = UTILITY_MENU_REMOVE_NODE_URL;
                moveUrl = UTILITY_MENU_MOVE_NODE_URL;
                break;
            default:
                throw 'The type was not properly set. Unable to create the tree.';
        }

        elem.on('move_node.jstree', function (e, data) {
                // The move event is triggered when a node is moved within the same tree
                moveItem(data, moveUrl);
            })
            .on('copy_node.jstree', function (e, data) {
                // The copy event is triggered when moving a node to another tree
                moveItem(data, moveUrl);
            })
            .jstree({
                'core': {
                    'data': {
                        type: 'POST',
                        url: url,
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        data: JSON.stringify({ column: column })
                    },
                    "themes": { "stripes": true, "icons": true, "dots": true },
                    "check_callback": true,
                    "multiple": false
                },
                "plugins": ["contextmenu", "dnd", "types", "state", "wholerow"],
                "contextmenu": {
                    "items": {
                        'delete': {
                            "label": 'Remove',
                            'action': function (data) {
                                var inst = $.jstree.reference(data.reference),
                                    obj = inst.get_node(data.reference);

                                removeItem(inst, obj, removeUrl);
                            }
                        }
                    }
                },
                "types": {
                    "#": {
                        "valid_children": ["default", "root"]
                    },
                    "root": {
                        "valid_children": ["default", "root"]
                    },
                    "default": {
                        "valid_children": ["default", "root"]
                    }
                }
        });
    };

    var initialize = function () {
        $('.js-tree').each(function () {
            var elem = $(this);
            
            initTree(elem);
        });
    };

    var addNewTree = function (type) {
        var canCreateTree = true;
        var trees = $('.js-tree');
        trees.each(function () {
            if($(this).find('li').length === 0) {
                canCreateTree = false;
            }
        });

        if (!canCreateTree) {
            return;
        }

        var parentContainer = $('#treeContainer');
        var numTrees = $('.js-tree').length;
        var column = numTrees + 1;
        var newTreeHtml = "<div class='js-tree-wrapper'><div class='js-tree' data-column='" + column + "' data-type='" + type + "'></div></div>"

        parentContainer.append(newTreeHtml);

        var newTree = parentContainer.find('.js-tree[data-column=' + column + ']');

        initTree(newTree);
    };

    initialize();

    return {
        addNewTree: addNewTree
    };
    
})(jQuery);
