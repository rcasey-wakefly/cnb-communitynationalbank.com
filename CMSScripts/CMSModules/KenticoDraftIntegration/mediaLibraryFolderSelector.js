﻿'use strict';

cmsdefine(['jQuery'], function ($) {

    var mediaLibraryFolderSelector = function (config) {

        if (!(config
            && config.selectBtnId
            && config.cancelBtnId
            && config.dialogHiddenfieldId
            && config.selecttorDisplayFieldId
            && config.selectorValueHiddenFieldId
            && config.dialogContainerId
            && config.dialogBGClass
            && config.dialogWrapperClass
            && config.confirmBtnId
            && config.selectedTreeItemClass
            && config.treeItemClass
            && config.nameElementClass
            && config.choosePathAlert
            && config.treeId)) {
            throw new Error("mediaLibraryFolderSelector javascript module is not configured properly.");
        }

        this.selectedTreeItemClass = config.selectedTreeItemClass;
        this.nameClass = config.nameElementClass;
        this.choosePathAlert = config.choosePathAlert;

        this.$selectBtn = $('#' + config.selectBtnId);
        this.$confirmBtn = $('#' + config.confirmBtnId);
        this.$cancelBtn = $('#' + config.cancelBtnId);
        this.$dialogHiddenField = $('#' + config.dialogHiddenfieldId);
        this.$selectorDisplayField = $('#' + config.selecttorDisplayFieldId);
        this.$selectorValueHiddentField = $('#' + config.selectorValueHiddenFieldId);
        this.$tree = $('#' + config.treeId);

        var $dialogcontainer = $('#' + config.dialogContainerId);
        this.$dialogWrapper = $dialogcontainer.children('.' + config.dialogWrapperClass);
        this.$dialogBackGround = $dialogcontainer.children('.' + config.dialogBGClass);

        if (this.$selectBtn.length != 1) {
            throw new Error('mediaLibraryFolderSelector has found ' + this.$selectBtn.length + ' elements for $selectBtn.');
        }
        if (this.$confirmBtn.length != 1) {
            throw new Error('mediaLibraryFolderSelector has found ' + this.$confirmBtn.length + ' elements for $confirmBtn.');
        }
        if (this.$cancelBtn.length != 1) {
            throw new Error('mediaLibraryFolderSelector has found ' + this.$cancelBtn.length + ' elements for $cancelBtn.');
        }
        if (this.$dialogHiddenField.length != 1) {
            throw new Error('mediaLibraryFolderSelector has found ' + this.$dialogHiddenField.length + ' elements for $dialogHiddenField.');
        }
        if (this.$selectorDisplayField.length != 1) {
            throw new Error('mediaLibraryFolderSelector has found ' + this.$selectorDisplayField.length + ' elements for $selectorDisplayField.');
        }
        if (this.$selectorValueHiddentField.length != 1) {
            throw new Error('mediaLibraryFolderSelector has found ' + this.$selectorValueHiddentField.length + ' elements for $selectorValueHiddentField.');
        }
        if (this.$tree.length != 1) {
            throw new Error('mediaLibraryFolderSelector has found ' + this.$tree.length + ' elements for $tree.');
        }
        if (this.$dialogWrapper.length != 1) {
            throw new Error('mediaLibraryFolderSelector has found ' + this.$dialogWrapper.length + ' elements for $dialogWrapper.');
        }
        if (this.$dialogBackGround.length != 1) {
            throw new Error('mediaLibraryFolderSelector has found ' + this.$dialogBackGround.length + ' elements for $dialogBackGround.');
        }

        this.$dialogBackGround.width('100%');
        this.$dialogBackGround.height('100%');

        this.$treeItems = this.$tree.find('span.' + config.treeItemClass);

        this.showValue(this.$selectorValueHiddentField);

        this.hookUpEvents();
    };

    mediaLibraryFolderSelector.prototype.showValue = function (target) {
        try {
            var value = JSON.parse(target.val());
            this.$selectorDisplayField.val(value.path);
        } catch (e) {

        }
    }

    mediaLibraryFolderSelector.prototype.hookUpEvents = function () {
        var that = this;

        that.$selectBtn.click(function (event) {
            event.preventDefault();
            that.openDialog();
        });
        that.$cancelBtn.click(function (event) {
            event.preventDefault();
            that.closeDialog();
        });
        that.$treeItems.click(function (event) {
            event.preventDefault();
            event.stopPropagation();
            that.selectNode($(event.currentTarget));
        });
        that.$confirmBtn.click(function (event) {
            event.preventDefault();
            that.confirmDialog();
        });
        that.$selectorValueHiddentField.change(function (event) {
            that.showValue($(event.target));
        });
    };

    mediaLibraryFolderSelector.prototype.confirmDialog = function () {
        try {
            var value = JSON.parse(this.$dialogHiddenField.val());
            if (!value.path) {
                alert(this.choosePathAlert);
                return;
            }
            this.$selectorValueHiddentField.val(JSON.stringify(value));
            this.$selectorValueHiddentField.change();
            this.closeDialog();
        } catch (e) {
            throw new Error('A non-json value found in $dialogHiddenField.');
        }
    };

    mediaLibraryFolderSelector.prototype.selectNode = function (node) {
        this.$tree.find('.' + this.selectedTreeItemClass).removeClass(this.selectedTreeItemClass);
        node.addClass(this.selectedTreeItemClass);
        try {
            var value = JSON.parse(this.$dialogHiddenField.val());
            value.path = node.children('.Name').attr('data-value');
            this.$dialogHiddenField.val(JSON.stringify(value));
        } catch (e) {
            throw new Error('A non-json value found in $dialogHiddenField.');
        }
    };

    mediaLibraryFolderSelector.prototype.openDialog = function () {
        if (this.$dialogWrapper && this.$dialogBackGround) {
            showModalPopup(this.$dialogWrapper.attr('id'), this.$dialogBackGround.attr('id'));
        }
    };

    mediaLibraryFolderSelector.prototype.closeDialog = function () {
        if (this.$dialogWrapper && this.$dialogBackGround) {
            hideModalPopup(this.$dialogWrapper.attr('id'), this.$dialogBackGround.attr('id'));
        }
    };

    return mediaLibraryFolderSelector;
});