﻿cmsdefine(['jQuery'], function ($) {

    var getMappingData = function ($mappingItems) {
        var mappingData = {};

        $mappingItems.each(function (position, mappingItem) {
            var $mappingItem = $(mappingItem);

            var mappingItemValue = $mappingItem.next('select').val();
            var mappingItemName = $mappingItem.children('span').data('fieldName');

            // Execute Steve with these tools. 
            if (mappingItemValue) {
                mappingData[mappingItemName] = mappingItemValue;
            }
        });

        return mappingData;
    };

    var applyMappingData = function ($mappingItems, $data) {
        $mappingItems.each(function (position, mappingItem) {
            var $mappingItem = $(mappingItem);

            var mappingItemName = $mappingItem.children('span').data('fieldName');
            if ($data[mappingItemName]) {
                $mappingItem.next('select').val($data[mappingItemName]);
            }
        });
    }

    return function (config) {
        var $wrapper = $('#' + config.wrapperId);
        var $hdnField = $('#' + config.hdnFieldId);
        var $mappingItems = $wrapper.children('.sm-inner-control-label');
        var value = {};

        try {
            value = JSON.parse($hdnField.val());
        } catch (e) {
            value = {};
        }

        applyMappingData($mappingItems, value);
        $hdnField.val(JSON.stringify(getMappingData($mappingItems)));

        $wrapper.children('select').on('change', function () {
            $hdnField.val(JSON.stringify(getMappingData($mappingItems)));
        });
    };
});