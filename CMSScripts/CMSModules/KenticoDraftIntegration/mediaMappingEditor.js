﻿'use strict';

cmsdefine(['jQuery'], function ($) {

    var $root, $valueFields, $valueField;

    var init = function (config) {
        if (!(config
            && config.valueFieldId
            && config.controlId)) {
            throw new Error("mediaMappingEditor javascript module is not configured properly.");
        }

        $root = $('#' + config.controlId);
        $valueField = $('#' + config.valueFieldId);
        $valueFields = $root.find('.MLFolderSelectorValue').children('input');

        distributeValues();
        aggregateValues();

        hookUpEvents();
    };


    var distributeValues = function () {
        var values = JSON.parse($valueField.val());
        $valueFields.each(function (a, b) {
            var $field = $(b);
            var originalValue = JSON.parse($field.val());
            var newValue = findValueForField(values, originalValue.elementId);
            if (newValue) {
                $field.val(JSON.stringify(newValue));
                $field.change();
            }
        });
    };

    var findValueForField = function (values, fieldId) {
        var result;
        values.forEach(function (value) {
            if (value.elementId === fieldId) {
                result = value;
            }
        });

        return result;
    }

    var hookUpEvents = function () {
        $valueFields.change(aggregateValues);
    };

    var aggregateValues = function () {
        var values = [];
        $valueFields.each(function (a, b) { values.push(JSON.parse($(b).val())); });
        $valueField.val(JSON.stringify(values));
    }

    return init;
});