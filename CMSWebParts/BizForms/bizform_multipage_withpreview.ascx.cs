using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using App.Helpers;
using CMS.Base;
using CMS.DataEngine;
using CMS.FormControls;
using CMS.FormEngine;
using CMS.Helpers;
using CMS.Localization;
using CMS.OnlineForms;
using CMS.PortalControls;
using CMS.SiteProvider;
using CMS.WebAnalytics;
using DocumentFormat.OpenXml.Spreadsheet;

public partial class CMSWebParts_BizForms_bizform_multipage_withpreview : CMSAbstractWebPart
{
    #region "Properties"

    /// <summary>
    /// Gets or sets the form name of BizForm.
    /// </summary>
    public string BizFormName
    {
        get
        {
            return ValidationHelper.GetString(GetValue("BizFormName"), "");
        }
        set
        {
            SetValue("BizFormName", value);
        }
    }


    /// <summary>
    /// Gets or sets the alternative form full name (ClassName.AlternativeFormName).
    /// </summary>
    public string AlternativeFormName
    {
        get
        {
            return ValidationHelper.GetString(GetValue("AlternativeFormName"), "");
        }
        set
        {
            SetValue("AlternativeFormName", value);
        }
    }


    /// <summary>
    /// Gets or sets the site name.
    /// </summary>
    public string SiteName
    {
        get
        {
            return ValidationHelper.GetString(GetValue("SiteName"), "");
        }
        set
        {
            SetValue("SiteName", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether the WebPart use colon behind label.
    /// </summary>
    public bool UseColonBehindLabel
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("UseColonBehindLabel"), true);
        }
        set
        {
            SetValue("UseColonBehindLabel", value);
        }
    }


    /// <summary>
    /// Gets or sets the message which is displayed after validation failed.
    /// </summary>
    public string ValidationErrorMessage
    {
        get
        {
            return ValidationHelper.GetString(GetValue("ValidationErrorMessage"), "");
        }
        set
        {
            SetValue("ValidationErrorMessage", value);
        }
    }


    /// <summary>
    /// Gets or sets the conversion track name used after successful registration.
    /// </summary>
    public string TrackConversionName
    {
        get
        {
            return ValidationHelper.GetString(GetValue("TrackConversionName"), "");
        }
        set
        {
            if (value.Length > 400)
            {
                value = value.Substring(0, 400);
            }
            SetValue("TrackConversionName", value);
        }
    }


    /// <summary>
    /// Gets or sets the conversion value used after successful registration.
    /// </summary>
    public double ConversionValue
    {
        get
        {
            return ValidationHelper.GetDoubleSystem(GetValue("ConversionValue"), 0);
        }
        set
        {
            SetValue("ConversionValue", value);
        }
    }

    /// <summary>
    /// Gets or sets the boolean if the form is a multipage form
    /// </summary>
    public bool IsMultiPageForm
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("IsMultiPageForm"), false);
        }
        set
        {
            SetValue("IsMultiPageForm", value);
        }
    }

    /// <summary>
    /// Gets or sets the boolean if the form is the first page of a multipage form
    /// </summary>
    public bool IsFirstPageOfMultiPageForm
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("IsFirstPageOfMultiPageForm"), false);
        }
        set
        {
            SetValue("IsFirstPageOfMultiPageForm", value);
        }
    }
    

    /// <summary>
    /// Gets or sets the boolean if the form is the last page of a multipage form
    /// </summary>
    public bool IsLastPageOfMultiPageForm
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("IsLastPageOfMultiPageForm"), false);
        }
        set
        {
            SetValue("IsLastPageOfMultiPageForm", value);
        }
    }

    /// <summary>
    /// Gets or sets the boolean if the form is the last page of a multipage form
    /// </summary>
    public string NextPage
    {
        get
        {
            return ValidationHelper.GetString(GetValue("NextPage"), "");
        }
        set
        {
            SetValue("NextPage", value);
        }
    }

    /// <summary>
    /// Gets or sets the boolean if the form is the last page of a multipage form
    /// </summary>
    public string PreviousPage
    {
        get
        {
            return ValidationHelper.GetString(GetValue("PreviousPage"), "");
        }
        set 
        {
            SetValue("PreviousPage", value);
        }
    }

    public string NextFormName
    {
        get
        {
            return ValidationHelper.GetString(GetValue("NextFormName"), "");
        }
        set
        {
            SetValue("NextFormName", value);
        }
    }

    public string PreviousFormName
    {
        get
        {
            return ValidationHelper.GetString(GetValue("PreviousFormName"), "");
        }
        set
        {
            SetValue("PreviousFormName", value);
        }
    }

    public string CustomTableName
    {
        get
        {
            //remove all whitesapce bc fuck you kentico
            return String.Join("", ValidationHelper.GetString(GetValue("CustomTableName"), "").Where(c => !char.IsWhiteSpace(c)));
        }
        set
        {
            SetValue("CustomTableName", value);
        }
    }


    public int CustomTableRowId
    {
        get
        {
            var rowid = 0;
            if (HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session[CustomTableName] != null)
            {
                Int32.TryParse(HttpContext.Current.Session[CustomTableName].ToString(), out rowid);
            }
            return rowid;
        }
        set
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
            {
                HttpContext.Current.Session[CustomTableName] = value;
            }
        }
    }

    #endregion


    #region "Methods"

    protected override void OnLoad(EventArgs e)
    {
        viewBiz.OnAfterSave += viewBiz_OnAfterSave;
        viewBiz.SubmitButton.Visible = false;
        //viewBiz.SubmitButton = btnNext;

        btnPrevious.Visible = btnPrevious.Enabled = !IsFirstPageOfMultiPageForm;
        btnPrevious.CssClass = viewBiz.SubmitButton.CssClass;
        
        if(IsLastPageOfMultiPageForm)
        {
            btnPrevious.Text = "Back";
        }

        //viewBiz.SubmitButton.Text = IsLastPageOfMultiPageForm ? "Preview & Print" : "Next";
        //viewBiz.ReloadData();

        var bizFormInfo = BizFormInfoProvider.GetBizFormInfo(BizFormName, SiteContext.CurrentSiteName);
        btnNext.Text = bizFormInfo.FormSubmitButtonText;
        if (!IsPostBack)
        {
            TryPopulateForm();
        }

        base.OnLoad(e);
    }


    private void TryPopulateForm()
    {
        /*if customtablerowid > 0
         * get row, try to fill form with values
         
         */
        if (CustomTableRowId <= 0) return;

        var row = CustomTableHelper.GetItemById(CustomTableName, CustomTableRowId);
        if (row == null) return;

        foreach (var column in viewBiz.Data.ColumnNames)
        {
            if (row[column] != null && viewBiz.FieldControls[column] != null)
            {
                viewBiz.FieldControls[column].Value = row[column];
            }
        }
    }

    public enum FormButton { Next, Previous }
 

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();        
    }


    /// <summary>
    /// Reloads data for partial caching.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (StopProcessing)
        {
            // Do nothing
            viewBiz.StopProcessing = true;
        }
        else
        {
            // Set BizForm properties
            viewBiz.FormName = BizFormName;
            viewBiz.SiteName = SiteName;
            viewBiz.UseColonBehindLabel = UseColonBehindLabel;
            viewBiz.AlternativeFormFullName = AlternativeFormName;
            viewBiz.ValidationErrorMessage = ValidationErrorMessage;           

            // Set the live site context
            if (viewBiz != null)
            {
                viewBiz.ControlContext.ContextName = CMS.ExtendedControls.ControlContext.LIVE_SITE;
            }
        }
    }


    private void viewBiz_OnAfterSave(object sender, EventArgs e)
    {
        var redirectPage = IsLastPageOfMultiPageForm ? viewBiz.FormRedirectToUrl : NextPage;
        SaveCurrentForm(redirectPage, FormButton.Next);

        if (TrackConversionName != String.Empty)
        {
            string siteName = SiteContext.CurrentSiteName;

            if (AnalyticsHelper.AnalyticsEnabled(siteName) && AnalyticsHelper.TrackConversionsEnabled(siteName) && !AnalyticsHelper.IsIPExcluded(siteName, RequestContext.UserHostAddress))
            {
                HitLogProvider.LogConversions(SiteContext.CurrentSiteName, LocalizationContext.PreferredCultureCode, TrackConversionName, 0, ConversionValue);
            }
        }

        if(IsLastPageOfMultiPageForm)
        {
            CustomTableRowId = 0;
        }
    }

   

    public void ShowPreview()
    {
        previewTable.Rows.Clear();
        var customTableRow = CustomTableHelper.GetItemById(CustomTableName, CustomTableRowId);
        if (customTableRow == null) return;

        var tRow = new TableRow();
        tRow.Cells.Add(new TableCell() { Text = "<h3>Preview of your data</h3><br><br><br>" });
        previewTable.Rows.Add(tRow);

        foreach (var columnName in customTableRow.ColumnNames)
        {
            string label = "", value = "";
            var dataCassInfo = DataClassInfoProvider.GetDataClassInfo(customTableRow.DataClassInfo.ClassID);
            var formInfo = new FormInfo(dataCassInfo.ClassFormDefinition);
            FormFieldInfo columnFieldInfo = formInfo.GetFormField(columnName);
            if (columnFieldInfo == null || !columnFieldInfo.Visible)
            {
                continue;
            }
            label = columnFieldInfo.Caption;
            
            if (customTableRow[columnName] != null)
            {
                value = customTableRow[columnName].ToString();
            }
            if (!string.IsNullOrWhiteSpace(label) && columnFieldInfo.Visible)
            {
                var fieldRow = new TableRow();
                fieldRow.Cells.Add(new TableCell() { Text = String.Format("<p><strong>{0}:</strong> <span>{1}</span></p>", label, value) });
                previewTable.Rows.Add(fieldRow);
            }
        }
        preview.Visible = true;
        viewBiz.Visible = false;
        btnNext.Visible = false;
        btnPrevious.Visible = false;
        StopProcessing = true;
    }

    public void HidePreview()
    {
        preview.Visible = false;
        viewBiz.Visible = true;
        btnNext.Visible = true;
        btnPrevious.Visible = true;
    }

    protected void btnPrevious_OnClick(object sender, EventArgs e)
    {
        //save current info
        //
        SaveCurrentForm(PreviousPage, FormButton.Previous);
        viewBiz.SaveData(PreviousPage);
        
    }

    private void SaveCurrentForm(string redirectPage, FormButton Direction)
    {
        //if row id of custom table is in session, get it and pass to addtocustomtable
        var dict = new Dictionary<string, string>();
        foreach (var columnName in viewBiz.Data.ColumnNames)
        {   
            FormEngineUserControl item = viewBiz.FieldControls[columnName];
            if (item != null && !(item is CMSFormControls_Media_UploadControl))
            {
                //(item as CMSFormControls_Media_UploadControl)
                dict.Add(columnName, item.Value.ToString());
            }
            else if (item is CMSFormControls_Media_UploadControl)
            {
                var bizFormInfo = BizFormInfoProvider.GetBizFormInfo(BizFormName, SiteContext.CurrentSiteID);
                if (bizFormInfo != null)
                {
                    DataClassInfo formClass = DataClassInfoProvider.GetDataClassInfo(bizFormInfo.FormClassID);
                    if (formClass != null)
                    {
                        var tableName = formClass.ClassName;
                        var currentItem = BizFormItemProvider.GetItem(viewBiz.ItemID, tableName);
                        if (currentItem != null)
                        {
                            dict.Add(columnName, currentItem.GetStringValue("UploadResume", ""));
                        }
                    }
                }
            }
        }
        var row = CustomTableHelper.CreateDataRow(dict);//"customtable.cnbloanapp"
        var Dci = DataClassInfoProvider.GetDataClassInfo(CustomTableName);
        CustomTableRowId = CustomTableHelper.AddToCustomTable(Dci, row, CustomTableRowId);

        if (IsLastPageOfMultiPageForm && Direction.Equals(FormButton.Next))
        {
            ShowPreview();
        }
    }

    protected void btnNext_OnClick(object sender, EventArgs e)
    {
        var redirectPage = IsLastPageOfMultiPageForm ? viewBiz.FormRedirectToUrl : NextPage;
        viewBiz.SaveData(redirectPage);
    }

    protected void btnSubmit_OnClick(object sender, EventArgs e)
    {
        preview.Visible = false;
        viewBiz.SaveData(viewBiz.RedirectUrlAfterSave);
    }
    
    protected void btnEdit_OnClick(object sender, EventArgs e)
    {
        HidePreview();
    }
    #endregion
}