using System;
using System.Web;
using System.Web.UI.WebControls;
using CMS.Base;
using CMS.FormControls;
using CMS.FormEngine;
using CMS.Helpers;
using CMS.Localization;
using CMS.OnlineForms;
using CMS.PortalControls;
using CMS.SiteProvider;
using CMS.WebAnalytics;

public partial class CMSWebParts_BizForms_bizform_withpreview : CMSAbstractWebPart
{
    #region "Properties"

    /// <summary>
    /// Gets or sets the form name of BizForm.
    /// </summary>
    public string BizFormName
    {
        get
        {
            return ValidationHelper.GetString(GetValue("BizFormName"), "");
        }
        set
        {
            SetValue("BizFormName", value);
        }
    }


    /// <summary>
    /// Gets or sets the alternative form full name (ClassName.AlternativeFormName).
    /// </summary>
    public string AlternativeFormName
    {
        get
        {
            return ValidationHelper.GetString(GetValue("AlternativeFormName"), "");
        }
        set
        {
            SetValue("AlternativeFormName", value);
        }
    }


    /// <summary>
    /// Gets or sets the site name.
    /// </summary>
    public string SiteName
    {
        get
        {
            return ValidationHelper.GetString(GetValue("SiteName"), "");
        }
        set
        {
            SetValue("SiteName", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether the WebPart use colon behind label.
    /// </summary>
    public bool UseColonBehindLabel
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("UseColonBehindLabel"), true);
        }
        set
        {
            SetValue("UseColonBehindLabel", value);
        }
    }


    /// <summary>
    /// Gets or sets the message which is displayed after validation failed.
    /// </summary>
    public string ValidationErrorMessage
    {
        get
        {
            return ValidationHelper.GetString(GetValue("ValidationErrorMessage"), "");
        }
        set
        {
            SetValue("ValidationErrorMessage", value);
        }
    }


    /// <summary>
    /// Gets or sets the conversion track name used after successful registration.
    /// </summary>
    public string TrackConversionName
    {
        get
        {
            return ValidationHelper.GetString(GetValue("TrackConversionName"), "");
        }
        set
        {
            if (value.Length > 400)
            {
                value = value.Substring(0, 400);
            }
            SetValue("TrackConversionName", value);
        }
    }


    /// <summary>
    /// Gets or sets the conversion value used after successful registration.
    /// </summary>
    public double ConversionValue
    {
        get
        {
            return ValidationHelper.GetDoubleSystem(GetValue("ConversionValue"), 0);
        }
        set
        {
            SetValue("ConversionValue", value);
        }
    }

    #endregion


    #region "Methods"

    protected override void OnLoad(EventArgs e)
    {
        //viewBiz.OnAfterSave += viewBiz_OnAfterSave;
        //viewBiz.OnBeforeSave += viewBiz_OnBeforeSave;

        viewBiz.OnValidationFailed += ViewBiz_OnValidationFailed;
        
        viewBiz.SubmitButton.Visible = false;

        var bizFormInfo = BizFormInfoProvider.GetBizFormInfo(BizFormName, SiteContext.CurrentSiteName);
        btnPreview.Text = bizFormInfo.FormSubmitButtonText;

        base.OnLoad(e);
    }

    private void ViewBiz_OnValidationFailed(object sender, EventArgs e)
    {
        btnEdit.Visible = false;
        btnSubmit.Visible = false;
        btnPreview.Visible = true;
    }

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();        
    }


    /// <summary>
    /// Reloads data for partial caching.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (StopProcessing)
        {
            // Do nothing
            viewBiz.StopProcessing = true;
        }
        else
        {
            //viewBiz.SubmitButton.Visible = false;
            // Set BizForm properties
            viewBiz.FormName = BizFormName;
            viewBiz.SiteName = SiteName;
            viewBiz.UseColonBehindLabel = UseColonBehindLabel;
            viewBiz.AlternativeFormFullName = AlternativeFormName;
            viewBiz.ValidationErrorMessage = ValidationErrorMessage;           

            // Set the live site context
            if (viewBiz != null)
            {
                viewBiz.ControlContext.ContextName = CMS.ExtendedControls.ControlContext.LIVE_SITE;
            }
        }
    }


    private void viewBiz_OnAfterSave(object sender, EventArgs e)
    {
        if (TrackConversionName != String.Empty)
        {
            string siteName = SiteContext.CurrentSiteName;

            if (AnalyticsHelper.AnalyticsEnabled(siteName) && AnalyticsHelper.TrackConversionsEnabled(siteName) && !AnalyticsHelper.IsIPExcluded(siteName, RequestContext.UserHostAddress))
            {
                HitLogProvider.LogConversions(SiteContext.CurrentSiteName, LocalizationContext.PreferredCultureCode, TrackConversionName, 0, ConversionValue);
            }
        }
    }

    #endregion

    protected void btnEdit_OnClick(object sender, EventArgs e)
    {
        pnlPreview.Visible = false;
        previewButtons.Visible = false;

        viewBiz.Visible = true;
        btnPreview.Visible = true;
    }

    protected void btnSubmit_OnClick(object sender, EventArgs e)
    {
        viewBiz.SaveData(viewBiz.RedirectUrlAfterSave);
        viewBiz.Visible = true;
        pnlPreview.Visible = false;
    }

    protected void btnPreview_OnClick(object sender, EventArgs e)
    {
        previewTable.Rows.Clear();
        btnPreview.Visible = false;

        btnEdit.Visible = true;
        btnSubmit.Visible = true;

        var editedObject = viewBiz.EditedObject as BizFormItem;
        var FormDisplayName = editedObject != null ? (viewBiz.EditedObject as BizFormItem).BizFormInfo.FormDisplayName : "";
        var tRow = new TableRow();
        tRow.Cells.Add(new TableCell() { Text = String.Format("<h3>Preview of your data</h3><br><strong>{0}</strong><br><br>", FormDisplayName) });
        previewTable.Rows.Add(tRow);

        foreach (var columnName in viewBiz.Data.ColumnNames)
        {
            string label = "", value = "";
            FormEngineUserControl item = viewBiz.FieldControls[columnName];
            var labelControl = viewBiz.FieldLabels[columnName];

            if (labelControl != null)
            {
                label = labelControl.Text;
            }
            if (item != null)
            {
                value = item.Value.ToString();
            }

            if (!string.IsNullOrWhiteSpace(label))
            {
                var fieldRow = new TableRow();
                fieldRow.Cells.Add(new TableCell() { Text = String.Format("<p><strong>{0}:</strong> <span>{1}</span></p>", label, value.Replace("\n", "<br/>")) });
                previewTable.Rows.Add(fieldRow);
            }
        }

        pnlPreview.Visible = true;
        previewButtons.Visible = true;

        viewBiz.Visible = false;
        StopProcessing = true;
    }
}