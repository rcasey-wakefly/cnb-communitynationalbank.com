<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_BizForms_bizform_multipage_withpreview" CodeFile="~/CMSWebParts/BizForms/bizform_multipage_withpreview.ascx.cs" %>
<%@ Reference Control="~/CMSFormControls/Media/UploadControl.ascx" %>

<cms:BizForm ID="viewBiz" runat="server" IsLiveSite="true" />

<asp:Panel runat="server" ID="preview" ClientIDMode="Static" Visible="False">
    <asp:Table runat="server" ID="previewTable">
    </asp:Table>
    <asp:Button runat="server" Text="Edit" ID="btnEdit" OnClick="btnEdit_OnClick"/>
    <asp:Button runat="server" Text="Submit" ID="btnSubmit" OnClick="btnSubmit_OnClick"/>
</asp:Panel>

<div>
    <asp:Button runat="server" Text="Prev" ID="btnPrevious" OnClick="btnPrevious_OnClick"/>
    <asp:Button runat="server" Text="Next" ID="btnNext" OnClick="btnNext_OnClick"/>
</div>

