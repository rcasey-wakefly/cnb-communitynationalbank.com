<%@ Control Language="C#" AutoEventWireup="true" Inherits="CMSWebParts_BizForms_bizform_withpreview" CodeFile="~/CMSWebParts/BizForms/bizform_withpreview.ascx.cs" %>

<cms:BizForm ID="viewBiz" runat="server" IsLiveSite="true" />

<asp:Panel runat="server" ID="pnlPreview" ClientIDMode="Static" Visible="False">
    <a href="/" onclick="PrintCtrl.printSelection(document.getElementById('preview'));return false" class="button">Print</a>
    <div id="preview">
        <label id="alert" class="text-error"><%= GlobalSettingsHelper.GetSettingsKeyStringValue("PreviewAlertText", string.Empty) %></label>
        <asp:Table runat="server" ID="previewTable">
        </asp:Table>
    </div>
</asp:Panel>

<div>
    <asp:Button runat="server" ID="btnPreview" OnClick="btnPreview_OnClick" CssClass="rsform-submit-button"/>

    <asp:Panel ID="previewButtons" runat="server" Visible="False">
        <asp:Button runat="server" Text="Back" ID="btnEdit" OnClick="btnEdit_OnClick" CssClass="rsform-submit-button"/>
        <asp:Button runat="server" Text="Send" ID="btnSubmit" OnClick="btnSubmit_OnClick" CssClass="rsform-submit-button"/>
    </asp:Panel>
</div>