﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CheckbookBalancer.ascx.cs" Inherits="CMSWebParts_FinancialCalculators_CheckbookBalancer_CheckbookBalancer" %>
<%@ Import Namespace="System.Web.Optimization" %>

<asp:Panel runat="server" ID="pnlWebpart">
    <asp:PlaceHolder runat="server" ID="stylePlaceholder">
        <%: Styles.Render("~/bundles/css/financialcalculators") %>
    </asp:PlaceHolder>

    <div class="KJEWrapper">
        <div class="KJEWidthConstraint">

            <h1>Checkbook Balancer</h1>

            <!--CALC_DESC-->

            Let us help you balance your checkbook. First tell us the ending balance on your statement, then enter all of your outstanding checks and deposits. If your checkbook register matches our calculated amount, your checkbook is balanced! If not, you may need to verify that all of your withdrawals and deposits are correct and accounted for.

            <!--CALC_DESC_END-->
            <noscript>
                <div align="center">
                    <div align="center" id="KJENoJavaScript" class="KJENoJavaScript">
                        Javascript is required for this calculator.  If you are using Internet Explorer, you may need to select to 'Allow Blocked Content' to view this calculator.
                        <p><b>For more information about these these financial calculators please visit: <a href="http://www.dinkytown.net" target="_blank">Financial Calculators</a> from KJE Computer Solutions, LLC</b></p>
                    </div>
                </div>
            </noscript>
            <div id="KJEAllContent"></div>
            <!--
  Financial Calculators, ©1998-2014 KJE Computer Solutions, LLC.
  For more information please see:
  <A HREF="http://www.dinkytown.net">http://www.dinkytown.net</A>
 -->

            <br>
            <hr size="1">
            <div align="center">
                <p class="KJEFooter">Information and interactive calculators are made available to you as self-help tools for your independent use and are not intended to provide investment advice. We cannot and do not guarantee their applicability or accuracy in regards to your individual circumstances. All examples are hypothetical and are for illustrative purposes.  We encourage you to seek personalized advice from qualified professionals regarding all personal finance issues. </p>
            </div>
        </div>
    </div>

    <asp:PlaceHolder runat="server" ID="scriptPlaceholder">
        <%: Scripts.Render("~/bundles/js/checkbook") %>
        
        <!--[if lt IE 9]>
    <%: Scripts.Render("~/bundles/js/excanvas") %>
<![endif]-->
    </asp:PlaceHolder>
</asp:Panel>
