﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.Helpers;
using CMS.PortalControls;

public partial class CMSWebParts_FinancialCalculators_CalculatorIndex : CMSAbstractWebPart
{
    public string Markup
    {
        get
        {
            return ValidationHelper.GetValue(GetValue("IndexMarkup"), string.Empty);
        }
        set
        {
            SetValue("IndexMarkup", value);
            litMarkup.Text = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected override void OnInit(EventArgs e)
    {
        litMarkup.Text = Markup;
        base.OnInit(e);
    }
}