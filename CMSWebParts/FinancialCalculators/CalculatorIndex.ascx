﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CalculatorIndex.ascx.cs" Inherits="CMSWebParts_FinancialCalculators_CalculatorIndex" %>
<%@ Import Namespace="System.Web.Optimization" %>

<asp:Panel runat="server" ID="pnlWebpart">
    <asp:PlaceHolder runat="server" ID="stylePlaceholder">
        <%: Styles.Render("~/bundles/css/financialcalculators") %>
    </asp:PlaceHolder>
    <div class="KJEWrapper">
        <div class="KJEWidthConstraint">

            <!-- \\Main Area\\ -->
            <h1>Financial Calculators</h1>

            <asp:Literal runat="server" ID="litMarkup"></asp:Literal>

            <div align="center">
                <p class="KJEFooter">Information and interactive calculators are made available to you as self-help tools for your independent use and are not intended to provide investment advice. We cannot and do not guarantee their applicability or accuracy in regards to your individual circumstances. All examples are hypothetical and are for illustrative purposes.  We encourage you to seek personalized advice from qualified professionals regarding all personal finance issues. </p>
            </div>
        </div>
    </div>
</asp:Panel>
