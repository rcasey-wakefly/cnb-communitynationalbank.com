using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalControls;
using CMS.DataEngine;
using CMS.Helpers;

public partial class CMSWebParts_CNB_CTAWidget : CMSAbstractWebPart
{
    #region "Properties"

    public string CTAText
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("CTAText"), string.Empty);
        }
        set
        {
            SetValue("CTAText", value);
        }
    }

    public string CTAImage
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("CTAImage"), string.Empty);
        }
        set
        {
            SetValue("CTAImage", value);
        }
    }

    public string CTALink
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("CTALink"), string.Empty);
        }
        set
        {
            SetValue("CTALink", value);
        }
    }

    public string CTALinkText
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("CTALinkText"), "Open An Account");
        }
        set
        {
            SetValue("CTALinkText", value);
        }
    }

    public bool NewWindow
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("NewWindow"), false);
        }
        set
        {
            SetValue("NewWindow", value);
        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {
            string CTABody = string.Empty;

            if (!String.IsNullOrEmpty(CTAText))
            {
                CTABody = "<p>" + CTAText + "</p>";
            }
            else if (!String.IsNullOrEmpty(CTAImage))
            {
                var imageURL = string.Empty;
                CTABody = "<img class='CTA-image' src='" + CTAImage + "' />";
            }
            CTA_left.InnerHtml = CTABody;

            CTA_link.HRef = CTALink;
            CTA_link.InnerHtml = CTALinkText + "<br />�";

            if (NewWindow)
            {
                CTA_link.Target = "_blank";
            }            
        }
    }


    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion
}



