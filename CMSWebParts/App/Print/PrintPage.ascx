﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PrintPage.ascx.cs" Inherits="CMSWebParts_App_Print_PrintPage" %>

<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="~/library/css/oldscreen.css"/>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/App/Content/library/js/vendor/jquery-1.10.2.min.js"><\/script>')</script>

<div class="contentpane modal">
    <div class="item-page" itemscope="" itemtype="http://schema.org/Article">
        <meta itemprop="inLanguage" content="en-GB">
        <a href="#" onclick="window.print();return false;"><span class="icon-print"></span>&nbsp;Print&nbsp;</a>
        <div id="pop-print" class="btn hidden-print">
            <a href="#" onclick="window.print();return false;"><i class="fa fa-print"></i>Print</a>
        </div>
        <div class="clearfix"> </div>
        <div id="printContent">
            <%--Content is loaded in here dynamically through javascript--%>
        </div>
    </div>
</div>

<script type="text/javascript">
    (function ($) {
        var getLocalStorage = function (key) {
            return localStorage.getItem(key);
        };

        var loadContent = function (containerId) {
            var printHtml = getLocalStorage("printHtml");

            var html = printHtml ? printHtml : "No content found.";

            var container = $(containerId);

            container[0].innerHTML = html;
        };

        loadContent('#printContent');
    })(jQuery);
</script>
