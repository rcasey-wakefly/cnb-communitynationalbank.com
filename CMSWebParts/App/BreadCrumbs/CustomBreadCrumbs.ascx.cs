﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using CMS.DocumentEngine;
using System.Text;
using CMS.PortalControls;

public partial class CMSWebParts_App_BreadCrumbs_CustomBreadCrumbs : CMSAbstractWebPart
{
    #region Properties

    public List<TreeNode> Pages = new List<TreeNode>();

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if (DocumentContext.CurrentDocument == null)
            {
                Response.Redirect("/");
            }

            CreateHierarchy(DocumentContext.CurrentDocument);

            Pages.Reverse();
            repBreadCrumbs.DataSource = Pages;
            repBreadCrumbs.DataBind();
        }
    }

    public string GetDisplayText(object dataItem)
    {
        if(dataItem == null)
        {
            return string.Empty;
        }

        var node = (TreeNode)dataItem;

        if(node == null)
        {
            return string.Empty;
        }

        var isLast = node == Pages.Last();
        var displayName = node.GetStringValue("DocumentMenuCaption", string.Empty);

        if (string.IsNullOrWhiteSpace(displayName))
        {
            displayName = node.DocumentName;
        }
        if(isLast)
        {
            return string.Format("<span class='last'>{0}</span>", displayName);
        }

        var crumbTextBuilder = new StringBuilder();

        crumbTextBuilder.Append(string.Format("<a href='{0}' class='pathway'>{1}</a>", node.NodeAliasPath, displayName));
        crumbTextBuilder.Append("<span class='divider'>»</span>");

        return crumbTextBuilder.ToString();
    }

    private void CreateHierarchy(TreeNode page)
    {
        if (page == null)
        {
            return;
        }

        Pages.Add(page);
        CreateHierarchy(page.Parent);
    }
}