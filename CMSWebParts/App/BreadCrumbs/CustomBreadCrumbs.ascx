﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomBreadCrumbs.ascx.cs" Inherits="CMSWebParts_App_BreadCrumbs_CustomBreadCrumbs" %>
<div id="breadcrumbs">
<div class="moduletable">
	<asp:Repeater ID="repBreadCrumbs" runat="server">
	    <HeaderTemplate>
	        <ul class="breadcrumb">
	    </HeaderTemplate>
		<ItemTemplate>
		    <li>
		        <asp:Literal runat="server" ID="linkText" Text="<%# GetDisplayText(Container.DataItem) %>"></asp:Literal>
		    </li>
		</ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
	</asp:Repeater>
</div>
    </div>