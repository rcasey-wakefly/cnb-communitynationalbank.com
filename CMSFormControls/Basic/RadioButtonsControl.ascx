<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RadioButtonsControl.ascx.cs"
    Inherits="CMSFormControls_Basic_RadioButtonsControl" %>
<fieldset>
    <legend>Radio Buttons</legend>
    <cms:CMSRadioButtonList ID="list" runat="server" />
</fieldset>
