﻿using System;
using System.Web.UI.WebControls;

using CMS.DataEngine;
using CMS.EventLog;
using CMS.FormControls;
using CMS.Helpers;

public partial class CMSFormControls_Filters_BooleanFilter : FormEngineUserControl
{
    #region "Variables"

    private string selectedValue = string.Empty;

    #endregion


    #region "Properties"

    /// <summary>
    /// Gets or sets value.
    /// </summary>
    public override object Value
    {
        get
        {
            if (!String.IsNullOrEmpty(drpConditionValue.SelectedValue))
            {
                return drpConditionValue.SelectedValue;
            }
            else
            {
                return DBNull.Value;
            }
        }
        set
        {
            if (ValidationHelper.IsBoolean(value))
            {
                selectedValue = ValidationHelper.GetBoolean(value, false) ? "1" : "0";
            }
            else
            {
                selectedValue = string.Empty;
            }
            drpConditionValue.SelectedValue = selectedValue;
        }
    }
    
    #endregion


    #region "Methods"

    protected override void CreateChildControls()
    {
        base.CreateChildControls();

        CheckFieldEmptiness = false;
        InitFilterDropDown();
    }


    /// <summary>
    /// Initializes filter dropdown list.
    /// </summary>
    private void InitFilterDropDown()
    {
        if (drpConditionValue.Items.Count == 0)
        {
            drpConditionValue.Items.Add(new ListItem(GetString("general.selectall"), string.Empty));
            drpConditionValue.Items.Add(new ListItem(GetString("general.yes"), "1"));
            drpConditionValue.Items.Add(new ListItem(GetString("general.no"), "0"));
            drpConditionValue.SelectedValue = selectedValue;
        }
    }


    /// <summary>
    /// Gets where condition.
    /// </summary>
    public override string GetWhereCondition()
    {
        EnsureChildControls();

        string tempVal = ValidationHelper.GetString(Value, null);

        // Only boolean value
        if (string.IsNullOrEmpty(tempVal) || !ValidationHelper.IsBoolean(tempVal))
        {
            return null;
        }

        bool value = ValidationHelper.GetBoolean(tempVal, false);

        if (String.IsNullOrEmpty(WhereConditionFormat))
        {
            return new WhereCondition(FieldInfo.Name, QueryOperator.Equals, value).ToString(true);
        }

        try
        {
            // Format where condition
            return String.Format(WhereConditionFormat, FieldInfo.Name, value ? "1" : "0", "=");
        }
        catch (Exception ex)
        {
            // Log exception
            EventLogProvider.LogException("BooleanFilter", "GetWhereCondition", ex);
        }

        return null;
    }

    #endregion
}