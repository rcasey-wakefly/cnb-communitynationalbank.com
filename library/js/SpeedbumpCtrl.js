﻿var SpeedbumpCtrl = (function ($) {
    var navConfirm = function (loc) {
        if (confirm('You are about to leave CommunityNationalBank.com and enter a linked site. The link is provided for your convenience and should not be considered as an endorsement of the products, services, or information provided, or an assurance of the security provided at the linked site. Do you want to continue to this webpage?')) {
            window.open(loc, '_blank');
        }
        return false; // cancel the click event always 
    };

    return {
        navConfirm: navConfirm
    };
})(jQuery);