﻿//General front end js
var frontEndCtrl = (function ($) {

    //adds extra space after a form field that has an explanation
    //can't do through css with the kentico generated markup
    function explanationSpace() {
        $('.ExplanationText').parent()
            .next('.form-table-group')
            .addClass('expSpace');
    }

    function init() {
        explanationSpace();
    }

    $(document).ready(init);

})(jQuery);

//alert
var alertJsController = (function () {

    var alert = $('.cnb-alert');

    function hasAlert() {
        if (alert.length) {
            $('#topmenu').addClass('has-alert');
        } 
    }

    function closeAlert() {
        $(document).on('click', '.close-alert', function () {
            alert.slideUp(350);
            setTimeout(function () {
                alert.remove();
                $('#topmenu').removeClass('has-alert');
            }, 350);
        });
    }

    function init() {
        hasAlert();
        closeAlert();
    }

    $(document).ready(init);

})();