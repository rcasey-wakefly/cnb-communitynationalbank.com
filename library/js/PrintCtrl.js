﻿var PrintCtrl = (function ($) {
    var setLocalStorage = function (key, value) {
        localStorage.setItem(key, value);
    };

    var openPrintWindow = function (url, containerId) {
        
        var container = $(containerId);
        var html = container[0].innerHTML;

        if (!html) {
            throw "The conent for the page was empty. Make sure the content you're trying to print is contained in a div with the class '.item-page'";
        }

        setLocalStorage("printHtml", html);

        window.open(url, 'win2', 'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no');

        return false;
    };


    var printSelection = function (node) {
        var content = node.innerHTML
        var pwin = window.open('_blank', 'print_content');
        pwin.document.open();
        pwin.document.write('<html><body onload="window.print()">' + content + '</body></html>');
        pwin.document.close();
        setTimeout(function () { pwin.close(); }, 1000);
    };

    return {
        openPrintWindow: openPrintWindow,
        printSelection: printSelection
    };
})(jQuery);