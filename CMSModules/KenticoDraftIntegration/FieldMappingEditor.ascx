﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FieldMappingEditor.ascx.cs" Inherits="KenticoDraftIntegration.CMSModules.KenticoDraftIntegration.FieldMappingEditor" %>

<asp:Label runat="server" ID="lblError" Visible="False" Style="margin-top: 6px; display: block;"></asp:Label>
<asp:HiddenField runat="server" ID="hdnMappings" EnableViewState="True" />
<asp:Panel runat="server" ID="pnlFields" />
