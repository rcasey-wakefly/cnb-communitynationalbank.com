﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportSingleDialog.aspx.cs" Inherits="CMSModules_KenticoDraftIntegration_ImportSingleDialog" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" %>

<%@ Register Src="~/CMSModules/KenticoDraftIntegration/CMSControls/SelectPath.ascx" TagName="SelectPath" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/KenticoDraftIntegration/FieldMappingEditor.ascx" TagPrefix="cms" TagName="FieldMappingEditor" %>
<%@ Register Src="~/CMSModules/KenticoDraftIntegration/AssetMappingEditor.ascx" TagPrefix="cms" TagName="AssetMappingEditor" %>


<asp:Content ID="Content" ContentPlaceHolderID="plcContent" runat="Server">
    <div class="form-horizontal">
        <h4>Map page type fields onto content entry elements</h4>
        <div class="form-group">
            <div class="editing-form-label-cell">
                <cms:LocalizedLabel runat="server" ID="lblSelectPath" CssClass="control-label" ResourceString="Page" DisplayColon="true" ShowRequiredMark="true"/>
            </div>
            <div class="editing-form-value-cell">
                <cms:SelectPath runat="server" ID="ctrlSelectPath" />
            </div>
        </div>
        <div class="form-group">
            <div class="editing-form-label-cell">
                <cms:LocalizedLabel runat="server" ID="lblMappingLabel" CssClass="control-label" ResourceString="Mapping" DisplayColon="true" ShowRequiredMark="true"/>
            </div>
            <div class="editing-form-value-cell">
                <cms:FieldMappingEditor runat="server" ID="ctrlFieldMappingEditor" />
            </div>
        </div>
        <asp:Panel runat="server" ID="pnlAssetMappings">
            <h4>Pick media libraries to import content entry assets to</h4>
            <div class="form-group">
                <div class="editing-form-label-cell">
                    <cms:LocalizedLabel runat="server" ID="lblAssetMappingLabel" CssClass="control-label" ResourceString="Media library paths" DisplayColon="true" ShowRequiredMark="true" />
                </div>
                <div class="editing-form-value-cell">
                    <cms:AssetMappingEditor runat="server" ID="ctrlMediaMappingEditor" />
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
