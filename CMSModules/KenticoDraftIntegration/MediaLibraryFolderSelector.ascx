﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MediaLibraryFolderSelector.ascx.cs" Inherits="KenticoDraftIntegration.CMSModules.KenticoDraftIntegration.MediaLibraryFolderSelector" %>
<%@ Register Src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" TagName="UniSelector" TagPrefix="cms" %>


<div class="control-group-inline">
    <cms:CMSTextBox type="text" disabled="disabled" class="form-control" runat="server" ID="txtValue" />
    <cms:CMSButton runat="server" ID="btnSelect" ButtonStyle="Default" Visible="True" UseSubmitBehavior="False" OnClientClick="return false;"></cms:CMSButton>
    <div style="display: none" class="MLFolderSelectorValue">
        <asp:HiddenField runat="server" ID="hdnValue" />
    </div>

    <asp:Panel runat="server" ID="dialogContainer">
        <cms:ModalPopupDialog ID="mdlDialog" runat="server" BackgroundCssClass="ModalBackground" CssClass="ModalPopupDialog" Visible="True" Width="800" Height="500">
            <asp:Panel runat="server" ID="pnlParameterPopup" Width="800" Height="500">
                <div class="dialog-header non-selectable">
                    <cms:LocalizedHeading Level="2" ID="titleElem" runat="server" CssClass="dialog-header-title" />
                </div>
                <cms:CMSUpdatePanel runat="server" ID="updForm">
                    <ContentTemplate>
                        <div class="dialog-content" style="bottom: 64px; position: absolute; top: 48px; width: 100%; background-color: #ffffff">
                            <div class="form-horizontal" style="margin-bottom: 0; display: inline-block;">
                                <div class="editing-form-label-cell">
                                    <cms:LocalizedLabel runat="server" ID="lblMLSelect" CssClass="control-label" ResourceString="ObjectType.media_library" DisplayColon="true" ShowRequiredMark="true" />
                                </div>
                                <div class="editing-form-value-cell">
                                    <cms:CMSDropDownList ID="frmMediaLibrary" runat="server" />
                                </div>
                                <div class="editing-form-label-cell">
                                    <cms:LocalizedLabel runat="server" ID="lblSelectPath" CssClass="control-label" ResourceString="general.folder" DisplayColon="true" ShowRequiredMark="true" />
                                </div>
                                <div class="editing-form-value-cell">
                                    <cms:LocalizedLabel runat="server" ID="lblError" Visible="False"></cms:LocalizedLabel>
                                    <div class="TreeArea TreeMain" style="position: relative; overflow: inherit; padding-top: 8px">
                                        <asp:TreeView ID="treeTree" runat="server" ShowLines="true" ShowExpandCollapse="true" EnableViewState="false">
                                            <HoverNodeStyle CssClass="HoveredFolder ContentTreeItem" />
                                            <RootNodeStyle CssClass="RootFolder ContentTreeItem" />
                                            <LeafNodeStyle CssClass="LeafFolder ContentTreeItem" />
                                            <NodeStyle CssClass="Folder ContentTreeItem" />
                                            <ParentNodeStyle CssClass="ParentFolder ContentTreeItem" />
                                            <SelectedNodeStyle CssClass="SelectedFolder ContentTreeSelectedItem" />
                                        </asp:TreeView>
                                    </div>
                                    <asp:HiddenField runat="server" ID="hdnDialogValue" />
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </cms:CMSUpdatePanel>
                <asp:Panel runat="server" ID="pnlFooter" CssClass="dialog-footer control-group-inline" Style="position: absolute; bottom: 0; width: 100%">
                    <cms:CMSButton runat="server" ID="btnCancel" ButtonStyle="Default" UseSubmitBehavior="False" OnClientClick="return false;" />
                    <cms:CMSButton runat="server" ID="btnDialogConfirm" ButtonStyle="Primary" UseSubmitBehavior="False" OnClientClick="return false;" />
                </asp:Panel>
            </asp:Panel>
        </cms:ModalPopupDialog>
    </asp:Panel>
</div>
