﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssetMappingEditor.ascx.cs" Inherits="KenticoDraftIntegration.CMSModules.KenticoDraftIntegration.MediaMappingEditor" %>


<cms:CMSUpdatePanel runat="server" ID="updFields">
    <ContentTemplate>
        <asp:Label runat="server" ID="lblError" Visible="False" Style="margin-top: 6px; display: block;"></asp:Label>
        <asp:Panel runat="server" ID="pnlFields"></asp:Panel>
        <asp:HiddenField runat="server" ID="hdnValue" EnableViewState="True" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
