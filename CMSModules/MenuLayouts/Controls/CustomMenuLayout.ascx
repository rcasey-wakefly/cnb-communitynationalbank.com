﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomMenuLayout.ascx.cs" Inherits="CMSApp.CMSModules.MenuLayouts.Controls.CustomMenuLayout" %>

<div id="pageWrapper">
    <div id="treeContainer">
        <asp:Literal runat="server" ID="litMenuLayout"></asp:Literal>
    </div>
</div>

