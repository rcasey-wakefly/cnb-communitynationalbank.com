﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CMSModules/MenuLayouts/Pages/MenuLayouts.master" AutoEventWireup="true" CodeBehind="FooterLayout.aspx.cs" Inherits="CMSApp.CMSModules.MenuLayouts.Pages.FooterLayout" Theme="Default"%>
<%@ Register src="~/CMSModules/MenuLayouts/Controls/CustomMenuLayout.ascx" tagPrefix="Menu" tagName="MenuLayout"%>

<asp:Content runat="server" ContentPlaceHolderID="plcActions">
    <div class="buttons">
        <input type="button" class="btn btn-primary" id="btnAddColumn" title="Add New Column" onclick="JsTreeCtrl.addNewTree('Footer')" value="Add New Column" />
    </div>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="plcContent" runat="server">
    <%--<MegaMenu:MenuLayout runat="server" ID="menuLayout" MenuType="MegaMenu"/>--%>
    <Menu:MenuLayout ID="menuLayout" runat="server" MenuType="Footer"/>
</asp:Content>
