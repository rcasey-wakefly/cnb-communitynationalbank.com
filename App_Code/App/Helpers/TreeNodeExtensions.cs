﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CMS.DocumentEngine;

namespace CMSApp.App.Classes.Helpers
{
    public static class TreeNodeExtensions
    {
        public static IEnumerable<TreeNode> GetChildren(this TreeNode node, string className, int maxRelativeLevel = 1)
        {
            return DocumentHelper.GetDocuments(className).Path(node.NodeAliasPath, PathTypeEnum.Children).NestingLevel(maxRelativeLevel);
        }

        public static IEnumerable<TreeNode> GetChildren(this TreeNode node, string className, int maxRelativeLevel = 1, params string[] columns)
        {
            return DocumentHelper.GetDocuments(className).Path(node.NodeAliasPath, PathTypeEnum.Children).NestingLevel(maxRelativeLevel).Columns(columns).OrderBy("NodeOrder");
        }
    }
}