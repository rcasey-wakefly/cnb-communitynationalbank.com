﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using CMS.CustomTables;
using CMS.DataEngine;
using CMS.UIControls.UniGridConfig;

namespace App.Helpers
{
    public static class CustomTableHelper
    {

        public static DataRow CreateDataRow<T>(T thisObject)
        {
            DataTable dt = new DataTable();
            foreach (PropertyInfo property in thisObject.GetType().GetProperties())
            {
                dt.Columns.Add(new DataColumn(property.Name, property.PropertyType));
            }

            DataRow newRow = dt.NewRow();
            foreach (PropertyInfo property in thisObject.GetType().GetProperties())
            {
                newRow[property.Name] = thisObject.GetType().GetProperty(property.Name).GetValue(thisObject, null);
            }

            return newRow;
        }

        public static DataRow CreateDataRow(Dictionary<string,string> thisObject)
        {
            DataTable dt = new DataTable();
            DataRow newRow = dt.NewRow();
            foreach (var item in thisObject)
            {      
                dt.Columns.Add(new DataColumn(item.Key));
                newRow[item.Key] = item.Value;
            }

            //foreach (PropertyInfo property in thisObject.GetType().GetProperties())
            //{
            //    dt.Columns.Add(new DataColumn(property.Name, property.PropertyType));
            //}

            //DataRow newRow = dt.NewRow();
            //foreach (PropertyInfo property in thisObject.GetType().GetProperties())
            //{
            //    newRow[property.Name] = thisObject.GetType().GetProperty(property.Name).GetValue(thisObject, null);
            //}


            return newRow;
        }

        public static int AddToCustomTable(DataClassInfo Dci, DataRow Row, int RowId = 0)
        {
            bool okInsert = false;
            if (RowId == 0)
            {
                // Creates new custom table item object
                var newCustomTableItem = CustomTableItem.New(Dci.ClassName, Row);

                // get the class definition
                var doc = XDocument.Parse(Dci.ClassFormDefinition);

                // get a list of all the tables properties by column value
                List<string> elements = doc.Descendants("field").Select(el => el.Attribute("column").Value).ToList();

                // got the class properties now compare
                foreach (string s in elements)
                {
                    // see if the class info contains the column in the data row passed in this method
                    if (Row.Table.Columns.Contains(s))
                    {
                        // contains the column so set a value
                        newCustomTableItem.SetValue(s, Row[s]);
                        okInsert = true;
                    }
                }
                if (okInsert)
                {
                    // Inserts the custom table item into database
                    newCustomTableItem.Insert();
                    return newCustomTableItem.ItemID;
                }
            }
            else
            {
                var existingRow = GetItemById(Dci.ClassName, RowId);
                if (existingRow != null)
                {
                    foreach (var col in Row.Table.Columns)
                    {
                        var column = (col as DataColumn);
                        if (column == null) return 0;
                        var columnName = column.ColumnName;
                        existingRow.SetValue(col.ToString(), Row[columnName]);
                    }
                    existingRow.Update();
                    return existingRow.ItemID;
                }
            }

            return 0;
        }

        public static List<CustomTableItem> GetItemsByType(string TableName, string Query)
        {
            var items = CustomTableItemProvider.GetItems(TableName, Query);
            return items != null ? items.ToList() : new List<CustomTableItem>();
        }

        public static List<CustomTableItem> GetItemsByType(string TableName, string Query, string orderBy = null, int topN = 0, string columns = null)
        {
            var items = CustomTableItemProvider.GetItems(TableName, Query, orderBy, topN, columns);
            return items != null ? items.ToList() : new List<CustomTableItem>();
        }

        public static List<CustomTableItem> GetItems(string tableName)
        {
            var items = CustomTableItemProvider.GetItems(tableName);
            return items != null ? items.ToList() : new List<CustomTableItem>();
        }

        public static CustomTableItem GetItemById(string tableName, int Id)
        {
            var query = string.Format("ItemID = {0}", Id);
            var item = CustomTableItemProvider.GetItems(tableName, query).FirstOrDefault();
            return item;
        }
    }
}