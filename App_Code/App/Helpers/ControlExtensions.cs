﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

public static class ControlExtensions
{
    public static IEnumerable<T> GetAllControlsOfType<T>(this Control container)
    {
        return container.GetAllControlsOfType<T>(new List<Control>());
    }

    private static IEnumerable<T> GetAllControlsOfType<T>(this Control container, List<Control> controls )
    {
        foreach (Control control in container.Controls)
        {
            if (control is T)
            {
                controls.Add(control);
            }
            if (control.Controls.Count > 0)
            {
                controls = control.GetAllControlsOfType<T>(controls).Cast<Control>().ToList();
            }
        }

        return controls.Cast<T>();
    }

    public static Control FindControlRecursive(this Control container, string controlId)
    {
        if(container.ID == controlId)
        {
            return container;
        }

        return container.Controls.Cast<Control>()
            .Select(con => con.FindControlRecursive(controlId))
            .FirstOrDefault(con => con != null);
    }
}
