﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using CMS.Helpers;

namespace CMSApp.App.Classes.Helpers
{
    public class ImgHelper
    {
        public static string GetImageUrl(object imageGuid) 
        {
	        if (imageGuid is Guid)
	        {
                return ImgHelper.GetImageUrl((Guid)imageGuid);
	        }
	        if (imageGuid is string)
	        {
                return ImgHelper.GetImageUrl(Guid.Parse(imageGuid as string));
	        }
	        else
	        {
	            //throw new Exception("Error in UrlHelper.GetRelativeImageUrl: 'imageGuid' is not a guid.");
                return ImgHelper.GetImageUrl(Guid.Empty);
	        }
	    }
	
	    private static string GetImageUrl(Guid imageGuid)
	    {
	        const string DefaultImageHandlerUrlFormat = "~/CMSPages/GetFile.aspx?guid={0}";
	        const string CustomImageHandlerUrlFormat = "~/Image.aspx?guid={0}";
	
	        // default to the native handler
	        string imageHandlerUrlFormat = DefaultImageHandlerUrlFormat;
	
	        // determine if the custom handler is enabled
	        bool isCustomImageHandlerEnabled = GlobalSettingsHelper.GetSettingsKeyBooleanValue("IsCustomImageHandlerEnabled");
	        if (isCustomImageHandlerEnabled) {
	            // use the custom handler
	            imageHandlerUrlFormat = CustomImageHandlerUrlFormat;
	        }

            return ImgHelper.ResolveUrl(string.Format(imageHandlerUrlFormat, imageGuid.ToString()));
	    }
	
	    public static string ResolveUrl(string originalUrl)
	    {
	        if (originalUrl == null)
	            return null;
	
	        // *** Absolute path - just return
	        if (originalUrl.IndexOf("://") != -1)
	            return originalUrl;
	
	        // *** Fix up image path for ~ root app dir directory
	        if (originalUrl.StartsWith("~"))
	        {
	            string newUrl = "";
	
	            if (HttpContext.Current != null)
	            {
	                if(HttpContext.Current.Handler is Page) {
	                    // try to use the page
	                    newUrl = (HttpContext.Current.Handler as Page).ResolveUrl(originalUrl);
	                }
	                else
	                {
	                    // use the virtual utility
	                    newUrl = VirtualPathUtility.ToAbsolute(originalUrl);
	                    // 
	                    //newUrl = HttpContext.Current.Request.ApplicationPath +
	                    //      originalUrl.Substring(1).Replace("//", "/");
	                }
	            }
	            else
	            {
                // *** Not context: assume current directory is the base directory
	                throw new ArgumentException("Invalid URL: Relative URL not allowed.");
	            }
	
	            // *** Just to be sure fix up any double slashes
	            return newUrl;
	        }
	
	        return originalUrl;
	    }

        public static string FullUrl(string url, HttpRequest request)
        {
            return request.Url.Scheme + Uri.SchemeDelimiter + request.Url.Authority + ImgHelper.ResolveUrl(url);
        }
    }
}