﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using CMS.DataEngine;
using CMS.SiteProvider;

public class GlobalSettingsHelper
{
    private const string KeyValueColumnName = "KeyValue";
	private const string CacheKeyPrefix = "GlobalSettingsHelper_";
        public static SettingsKeyInfo GetSettingsKeyInfo(string keyName) 
        {
	        if (String.IsNullOrEmpty(keyName)) 
            {
	            return null;
            }
	
	        string cacheKey = CacheKeyPrefix + keyName;
	        var info = HttpRuntime.Cache[cacheKey] as SettingsKeyInfo; // safer than HttpContext.Current.Cache which is sometimes null
	
	        if (info == null) {
	            // returns the info whether its a site specific key or a global one
	            info = SettingsKeyInfoProvider.GetSettingsKeyInfo(keyName, SiteContext.CurrentSiteID);
	            if (info == null || info.KeyValue == null) {
                    info = SettingsKeyInfoProvider.GetSettingsKeyInfo(keyName);
	            }
	
	            if (info != null) {
	                HttpRuntime.Cache.Insert(cacheKey, info, null, DateTime.Now.AddMinutes(5D), Cache.NoSlidingExpiration);
	            }
	        }
	
	        return info;
	    }

    public static bool GetSettingsKeyBooleanValue(string keyName, bool defaultValue = false)
    {
        var info = GetSettingsKeyInfo(keyName);
        if (info == null)
        {
            return defaultValue;
        }

        return info.GetBooleanValue(KeyValueColumnName, defaultValue);
    }

    public static DateTime GetSettingsKeyDateTimeValue(string keyName, DateTime defaultValue = default(DateTime))
    {
        var info = GetSettingsKeyInfo(keyName);
        if (info == null)
        {
            return defaultValue;
        }

        return info.GetDateTimeValue(KeyValueColumnName, defaultValue);
    }

    public static double GetSettingsKeyDoubleValue(string keyName, double defaultValue = 0D)
    {
        var info = GetSettingsKeyInfo(keyName);
        if (info == null)
        {
            return defaultValue;
        }

        return info.GetDoubleValue(KeyValueColumnName, defaultValue);
    }

    public static Guid GetSettingsKeyGuidValue(string keyName, Guid defaultValue = default(Guid))
    {
        var info = GetSettingsKeyInfo(keyName);
        if (info == null)
        {
            return defaultValue;
        }

        return info.GetGuidValue(KeyValueColumnName, defaultValue);
    }

    public static int GetSettingsKeyIntegerValue(string keyName, int defaultValue = 0)
    {
        var info = GetSettingsKeyInfo(keyName);
        if (info == null)
        {
            return defaultValue;
        }

        return info.GetIntegerValue(KeyValueColumnName, defaultValue);
    }

    public static string GetSettingsKeyStringValue(string keyName, string defaultValue = null)
    {
        var info = GetSettingsKeyInfo(keyName);
        if (info == null)
        {
            return defaultValue;
        }

        return info.GetStringValue(KeyValueColumnName, defaultValue);
    }

    public static object GetSettingsKeyValue(string keyName)
    {
        var info = GetSettingsKeyInfo(keyName);
        if (info == null)
        {
            return null;
        }

        return info.GetValue(KeyValueColumnName);
    }
}