﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;

public static class CacheService
{
    private static ObjectCache Cache { get { return MemoryCache.Default; } }

    public static bool Add(string key, object value, DateTimeOffset expiration)
    {
        return Cache.Add(key, value, expiration);
    }

    public static object Get(string key)
    {
        return Cache.Get(key);
    }

    public static bool CheckAdd(string key, object value, DateTimeOffset expiration)
    {
        var cacheItem = Cache.GetCacheItem(key);

        if (cacheItem != null)
        {
            Cache.Remove(key);
        }

        return Cache.Add(key, value, expiration);
    }

    public static void UpdateItem(string key, object newValue, DateTimeOffset expiration)
    {
        Cache.Set(key, newValue, expiration);
    }
}