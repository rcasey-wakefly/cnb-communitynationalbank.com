﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CMS.DocumentEngine;
using MenuLayouts.Helpers;
using MenuLayouts.Models;

/// <summary>
/// Summary description for MenuService
/// </summary>
public static class MenuService
{
	private static IEnumerable<TreeNode> GetMenuNodes(BaseMenuModel menuModel)
	{
        return DocumentHelper.GetDocuments(Constants.PageClassName)
                .Columns(Constants.MenuItemDisplayNameField, 
                        menuModel.ParentNodeField, 
                        menuModel.ColumnField, 
                        menuModel.OrderField, 
                        Constants.NodeGuidField, 
                        "NodeAliasPath",
                        menuModel.LinkTypeField,
                        menuModel.InternalLinkField,
                        menuModel.ExternalLinkField,
                        menuModel.ImageField)
                .WhereEquals(menuModel.ShowInMenuField, 1);
	}

    public static IEnumerable<MenuColumn> GetColumns(BaseMenuModel menuModel)
    {
        var allNodes = GetMenuNodes(menuModel);

        var cols = allNodes.Select(node => node.GetIntegerValue(menuModel.ColumnField, 0))
            .OrderBy(col => col)
            .Distinct();

        var columns = new List<MenuColumn>();

        if(!cols.Any())
        {
            return columns;
        }

        var rootNode = DocumentHelper.GetDocuments(Constants.RootPageClassName).FirstOrDefault();

        foreach (var column in cols)
        {
            columns.Add(allNodes
                .Where(node => node.GetIntegerValue(menuModel.ColumnField, 0) == column && 
                    node.GetStringValue(menuModel.ParentNodeField, string.Empty)
                    .Equals(rootNode.NodeGUID.ToString(), StringComparison.OrdinalIgnoreCase))
                .OrderBy(node => node.GetIntegerValue(menuModel.OrderField, 0))
                .Select(node => new MenuColumn(CreateMenuModelByType(menuModel.GetType(), node)))
                .FirstOrDefault());
        }

        return columns;
    }

    public static BaseMenuModel CreateMenuModelByType(Type baseModelType, TreeNode node)
    {
        var ctor = baseModelType.GetConstructor(new[] { node.GetType() });

        var inst = ctor.Invoke(new[] { node });

        return (BaseMenuModel)inst;
    }
}