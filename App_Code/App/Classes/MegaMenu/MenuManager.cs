﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using CMS.SiteProvider;
using MenuLayouts.MenuLayouts.Services;

public static class MenuManager
{
    public static int CacheMinutes { get { return GlobalSettingsHelper.GetSettingsKeyIntegerValue("MegaMenuCacheMinutes"); } }

    private static object _lock = new object();

    public static Menu CreateSiteMenu(HtmlGenericControl primaryNav, Page page, string userAgent)
    {
        string cacheKey = "mega_menu_" + SiteContext.CurrentSiteName;

        // try to get from cache
        var menu = MenuCacheService.Get(cacheKey) as Menu;

        if (menu == null)
        {
            // derive menu
            menu = new Menu();

            // store in cache
            // TODO: Add this setting to the MenuLayouts module
            var cacheMin = CacheMinutes;

            cacheMin = cacheMin > 0 ? cacheMin : 60;

            if(!menu.IsEmpty)
            {
                // Only cache the menu if it's not empty
                MenuCacheService.Add(cacheKey, menu, DateTime.Now.AddMinutes(cacheMin), TimeSpan.Zero);
            }

            AddControlsToMenu(menu, primaryNav, page);
        }
        else
        {
            AddControlsToMenu(menu, primaryNav, page);
        }

        return menu;
    }

    private static void AddControlsToMenu(Menu menu, HtmlGenericControl primaryNav, Page page)
    {
        if (menu != null && !menu.IsEmpty)
        {
            var controls = page.GetMenuControls(menu.Columns);
            foreach (Control control in controls)
            {
                primaryNav.Controls.Add(control);
            }
        }
    }

    private static ControlCollection GetMenuControls(this Page page, List<MenuColumn> columns)
    {
        string cacheKey = "mega_menu_controls_" + SiteContext.CurrentSiteName;

        var controls = MenuCacheService.Get(cacheKey) as ControlCollection;

        if (controls == null)
        {
            controls = new ControlCollection(page);

            foreach (var column in columns)
            {
                if (!string.IsNullOrEmpty(column.ControlPath))
                {
                    var linkControl = page.LoadControl(column.ControlPath) as BaseMenuControl;

                    if (linkControl != null)
                    {
                        linkControl.MenuLink = column;
                        linkControl.IsFirst = column == columns.First();
                        linkControl.IsLast = column == columns.Last();

                        controls.Add(linkControl);
                    }
                }
            }
        }
        //else
        //{
        //    foreach (var control in controls)
        //    {
        //        var bControl = control as BaseMenuControl;

        //        if (bControl != null)
        //        {
        //            bControl.IsActive = bControl.CheckIsActive();
        //        }
        //    }
        //}

        return controls;
    }
}