﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using CMS.DocumentEngine;
using CMS.EventLog;
using CMS.SiteProvider;
using MenuLayouts.MenuLayouts.Services;

/// <summary>
/// Summary description for BaseMenuControl
/// </summary>
public class BaseMenuControl : UserControl
{
    public MenuLink MenuLink { get; set; }
    public bool IsFirst { get; set; }
    public bool IsLast { get; set; }
    public bool IsActive { get; set; }

    public static int CacheMinutes { get { return GlobalSettingsHelper.GetSettingsKeyIntegerValue("MegaMenuCacheMinutes"); } }

    protected override void OnInit(EventArgs e)
    {
        if (!IsPostBack)
        {
            //IsActive = CheckIsActive();
        }

        base.OnInit(e);
    }

    public bool CheckIsActive()
    {
        if(MenuLink is MenuColumn)
        {
            var allPaths = MenuCacheService.Get(string.Format("{0}children", MenuLink.DisplayText.Replace(" ", string.Empty))) as IEnumerable<string>;

            if (allPaths == null)
            {
                allPaths = GetAllChildrenPaths(MenuLink);

                var cacheMin = CacheMinutes;

                cacheMin = cacheMin > 0 ? cacheMin : 60;

                MenuCacheService.Add(string.Format("{0}children", MenuLink.DisplayText.Replace(" ", string.Empty)), allPaths, DateTime.Now.AddMinutes(cacheMin), TimeSpan.Zero);
            }

            var paths = allPaths.ToArray();

            var isActive = false;

            try
            {
                isActive = MenuLink.NodeAliasPath.Equals(DocumentContext.CurrentDocument.NodeAliasPath, StringComparison.OrdinalIgnoreCase)
                || paths.Contains(DocumentContext.CurrentDocument.NodeAliasPath.ToLower())
                || paths.FirstOrDefault(path => path.IndexOf(DocumentContext.CurrentDocument.NodeAliasPath.ToLower()) >= 0) != null;
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("BaseMenuControl.CheckIsActive", 
                    "ISACTIVE_CHECK_FAILED", 
                    ex, 
                    SiteContext.CurrentSiteID, 
                    string.Format("Unable to set is active for MenuLink. MenuLink is Null {0}, CurrentDocument is Null {1}", 
                            MenuLink == null, 
                            DocumentContext.CurrentDocument == null));
            }
            
            return isActive;
        }

        return MenuLink.NodeAliasPath.Equals(DocumentContext.CurrentDocument.NodeAliasPath, StringComparison.OrdinalIgnoreCase);
    }

    private IEnumerable<string> GetAllChildrenPaths(MenuLink currentLink)
    {
        var paths = new List<string>();

        foreach (var child in currentLink.Children)
        {
            paths.Add(child.NodeAliasPath.ToLower());

            if(child.Children.Any())
            {
                paths.AddRange(GetAllChildrenPaths(child));
            }
        }

        return paths;
    }
}