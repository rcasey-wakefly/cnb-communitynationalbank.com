﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CMS.DocumentEngine;
using CMS.Helpers;
using MenuLayouts.Models;

public class MenuColumn : MenuLink
{
    public MenuColumn(BaseMenuModel menuModel)
        : base(menuModel)
	{
        
	}

    public override string GetControlPath()
    {
        return URLHelper.ResolveUrl("~/App/UserControls/MegaMenu/MenuColumn.ascx");
    }
}