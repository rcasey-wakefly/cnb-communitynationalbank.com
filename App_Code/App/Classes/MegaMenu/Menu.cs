﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CMS.DocumentEngine;
using MenuLayouts.Helpers;
using MenuLayouts.Models;

public class Menu
{
    public bool IsEmpty { get; private set; }

    public List<MenuColumn> Columns { get; set; }
	
	public Menu()
	{
	    SetColumns();
	}

    private void SetColumns()
    {
        var menuModel = new MegaMenu();

        Columns = MenuService.GetColumns(menuModel).ToList();

        IsEmpty = !Columns.Any();
    }
}