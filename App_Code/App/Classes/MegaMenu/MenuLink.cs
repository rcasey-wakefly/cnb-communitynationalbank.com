﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CMS.DocumentEngine;
using CMS.Helpers;
using MenuLayouts.Helpers;
using MenuLayouts.Models;

/// <summary>
/// Summary description for Link
/// </summary>
public class MenuLink
{
    public enum LinkTypes
    {
        Image = 1,
        InternalLink,
        ExternalLink,
        Header
    }

    public string Url { get; set; }
    public string DisplayText { get; set; }
    public string ImageUrl { get; set; }
    public string CssClass { get; set; }
    public string NodeAliasPath { get; set; }
    public string ControlPath { get; set; }

    public LinkTypes LinkType { get; set; }

    public IEnumerable<MenuLink> Children { get; set; }

    protected BaseMenuModel MenuModel;

    public MenuLink(BaseMenuModel menuModel)
	{
        MenuModel = menuModel;

        LinkType = (LinkTypes)MenuModel.LinkType;
        DisplayText = MenuModel.MenuItemDisplayName;
        Url = GetLinkUrl();
        ImageUrl = MenuModel.ImageUrl;
        CssClass = MenuModel.CssClass;
        NodeAliasPath = MenuModel.NodeAliasPath;
        ControlPath = GetControlPath();

        SetChildren();
	}

    private void SetChildren()
    {
        Guid parGuid;

        IEnumerable<MenuLink> nodes = null;

        if (Guid.TryParse(MenuModel.NodeGuid, out parGuid))
        {
             nodes = DocumentHelper.GetDocuments(Constants.PageClassName)
                .WhereEquals(MenuModel.ParentNodeField, parGuid)
                .WhereEquals(MenuModel.ShowInMenuField, 1)
                .OrderBy(MenuModel.OrderField)
                .Select(node => new MenuLink(MenuService.CreateMenuModelByType(MenuModel.GetType(), node)));
        }

        Children = nodes;
    }

    private string GetLinkUrl()
    {
        switch (LinkType)
        {
            case LinkTypes.Image:
                return URLHelper.ResolveUrl(MenuModel.NodeAliasPath);
            case LinkTypes.InternalLink:
                return URLHelper.ResolveUrl(MenuModel.InternalLink);
            case LinkTypes.ExternalLink:
                return MenuModel.ExternalLink;
            case LinkTypes.Header:
                return string.Empty;
            default:
                return "#unknown-url-error";
        }
    }

    public virtual string GetControlPath()
    {
        switch (LinkType)
        {
            case LinkTypes.Image:
                return URLHelper.ResolveUrl("~/App/UserControls/MegaMenu/MenuImageLink.ascx");
            case LinkTypes.InternalLink:
            case LinkTypes.ExternalLink:
                return URLHelper.ResolveUrl("~/App/UserControls/MegaMenu/MenuLink.ascx");
            case LinkTypes.Header:
                return URLHelper.ResolveUrl("~/App/UserControls/MegaMenu/MenuHeader.ascx");
            default:
                return "#unknown-url-error";
        }
    }
}