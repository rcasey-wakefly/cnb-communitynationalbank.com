﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CMS.DocumentEngine;
using CMSApp.App.Classes.Helpers;

/// <summary>
/// Summary description for BaseHero
/// </summary>
public class BaseHero : System.Web.UI.UserControl
{
    public TreeNode CurrentDocument { get; set; }
    public string Image { get; set; }
    public string ImagePath { get; set; }
    public string ImageAltText { get; set; }
    public string Header { get; set; }
    public string SubHeader { get; set; }
    public string Description { get; set; }
    public string Link { get; set; }
    public string ButtonText { get; set; }

    public TreeNode Hero { get; set; }
    public const string Cover = "cover";
    public const string BackgroundExtra = " no-repeat center center scroll";

    protected override void OnInit(EventArgs e)
    {
        CurrentDocument = DocumentContext.CurrentDocument;
        if (CurrentDocument == null) return;

        Hero = DocumentHelper.GetDocuments("Common.Hero").Path(CurrentDocument.NodeAliasPath, PathTypeEnum.Children).FirstOrDefault();
        if (Hero == null) return;

        Image = Hero.GetStringValue("Image", "");
        ImageAltText = Hero.GetStringValue("ImageAltText", "");
        ImagePath = ImagePath = ImgHelper.FullUrl(Image, Request);
        Header = this.Hero.GetStringValue("HeaderText", "");
        SubHeader = this.Hero.GetStringValue("SubHeaderText", "");
        Description = this.Hero.GetStringValue("Description", "");
        Link = this.Hero.GetStringValue("Link", "");
        ButtonText = this.Hero.GetStringValue("ButtonText", "");

    }
}