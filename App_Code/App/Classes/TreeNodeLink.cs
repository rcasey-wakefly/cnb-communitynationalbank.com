﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Storage.Blob.Protocol;
using TreeNode = CMS.DocumentEngine.TreeNode;

namespace CMSApp.App.Classes
{
    public class TreeNodeLink
    {
        public string DocumentName { get; set; }
        public List<TreeNodeLink> Children { get; set; }
        public TreeNode Node { get; set; }
        public string NodeAliasPath { get; set; }
        public bool ShowChildren { get; set; }
        public MenuLink.LinkTypes LinkType { get; set; }
    }
}