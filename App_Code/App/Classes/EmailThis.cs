﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using CMS.Base;
using CMS.DataEngine;
using CMS.EmailEngine;
using CMS.EventLog;
using CMS.Helpers;
using CMS.MacroEngine;
using CMS.Membership;
using CMS.Newsletters;
using CMS.OnlineForms;
using CMS.SiteProvider;

namespace CMSApp.App.Classes
{
    public static class EmailThis
    {
        public static bool ShareEmail(string to, string from, string subject, string body)
        {
            try
            {
                var em = new EmailMessage();
                em.EmailFormat = EmailFormatEnum.Both;
                em.Recipients = to;
                em.From = from;
                em.Subject = subject;
                em.Body = body;

                EmailSender.SendEmail(em);
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("EmailThis.ShareEmail", "SHARED_EMAIL_FAILED", ex);
                return false;
            }

            return true;
        }
    }
}