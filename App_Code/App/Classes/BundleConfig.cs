﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

/// <summary>
/// Summary description for BundleConfig
/// </summary>
public abstract class BundleConfig
{
    public static void RegisterBundles()
    {
        BundleCollection bundles = BundleTable.Bundles;

        bundles.Add(new ScriptBundle("~/bundles/js/main")
            .Include("~/library/js/SpeedbumpCtrl.js", 
                    "~/library/js/PrintCtrl.js",
                    "~/library/js/FrontEndCtrl.js"));

        bundles.Add(new StyleBundle("~/bundles/css/main")
            .Include("~/library/css/oldscreen.css"));

        bundles.Add(new StyleBundle("~/bundles/css/financialcalculators")
           .Include("~/library/css/FinancialCalculators/KJE.css",
                    "~/library/css/FinancialCalculators/KJESiteSpecific.css"));


        bundles.Add(new ScriptBundle("~/bundles/js/excanvas").Include("~/library/js/excanvas.js"));

        bundles.Add(new ScriptBundle("~/bundles/js/checkbook")
            .Include("~/library/js/FinancialCalculators/KJE.js",
                    "~/library/js/FinancialCalculators/KJESiteSpecific.js", 
                    "~/library/js/FinancialCalculators/CheckbookBalancer/CheckBook.js",
                    "~/library/js/FinancialCalculators/CheckbookBalancer/CheckBookParams.js"));

        bundles.Add(new ScriptBundle("~/bundles/js/payoff")
            .Include("~/library/js/FinancialCalculators/KJE.js",
                    "~/library/js/FinancialCalculators/KJESiteSpecific.js", 
                    "~/library/js/FinancialCalculators/CreditCardPayOff/PayoffCC.js",
                    "~/library/js/FinancialCalculators/CreditCardPayOff/PayoffCCParams.js"));

        bundles.Add(new ScriptBundle("~/bundles/js/homebudget")
            .Include("~/library/js/FinancialCalculators/KJE.js",
                    "~/library/js/FinancialCalculators/KJESiteSpecific.js", 
                    "~/library/js/FinancialCalculators/HomeBudgetAnalysis/HomeBudget.js",
                    "~/library/js/FinancialCalculators/HomeBudgetAnalysis/HomeBudgetParams.js"));

        bundles.Add(new ScriptBundle("~/bundles/js/savings")
            .Include("~/library/js/FinancialCalculators/KJE.js",
                    "~/library/js/FinancialCalculators/KJESiteSpecific.js", 
                    "~/library/js/FinancialCalculators/SavingsGoals/Savings.js",
                    "~/library/js/FinancialCalculators/SavingsGoals/SavingsParams.js"));

        bundles.Add(new ScriptBundle("~/bundles/js/studentbudget")
            .Include("~/library/js/FinancialCalculators/KJE.js",
                    "~/library/js/FinancialCalculators/KJESiteSpecific.js", 
                    "~/library/js/FinancialCalculators/StudentBudget/StudentBudget.js",
                    "~/library/js/FinancialCalculators/StudentBudget/StudentBudgetParams.js"));
    }
}