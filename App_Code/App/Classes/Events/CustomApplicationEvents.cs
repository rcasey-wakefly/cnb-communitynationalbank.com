﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CMS.Base;
using CMS.SiteProvider;
using MenuLayouts.MenuLayouts.Services;

[CustomApplicationEvents]
public partial class CMSModuleLoader
{
     /// <summary>
    /// Summary description for CustomApplicationEvents
    /// </summary>
    private class CustomApplicationEvents : CMSLoaderAttribute
    {
         public override void Init()
         {
             base.Init();
             ApplicationEvents.Initialized.Execute += Initialized_Execute;

             MenuCacheService.CacheInvalidated += MenuCacheServiceOnCacheInvalidated;
         }

         private void MenuCacheServiceOnCacheInvalidated()
         {
             MenuCacheService.Invalidate("mega_menu_" + SiteContext.CurrentSiteName);
             MenuCacheService.Invalidate("mega_menu_controls_" + SiteContext.CurrentSiteName);
             MenuCacheService.Invalidate("mobile_menu_" + SiteContext.CurrentSiteName);
         }

         void Initialized_Execute(object sender, EventArgs e)
         {
             BundleConfig.RegisterBundles();
         }
    }
}
