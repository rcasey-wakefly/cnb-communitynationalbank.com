﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Breadcrumbs.ascx.cs" Inherits="CMSApp.App.UserControls.Breadcrumbs" %>

<div class="breadcrumbs">
	<asp:Repeater ID="repBreadCrumbs" runat="server" OnItemDataBound="repBreadCrumbs_ItemDataBound">
		<ItemTemplate>
			<asp:Literal ID="ContainerOpen" runat="server"></asp:Literal>
				<asp:HyperLink ID="LinkTarget" runat="server">
					<asp:Literal ID="LinkText" runat="server"></asp:Literal>
				</asp:HyperLink>
			<asp:Literal ID="ContainerClose" runat="server"></asp:Literal>
		</ItemTemplate>
		<SeparatorTemplate>
			<i class="fa fa-angle-right results-seperator"></i>
		</SeparatorTemplate>
	</asp:Repeater>
</div>