﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Callout.ascx.cs" Inherits="CMSApp.App.UserControls.Callout" %>
<%@Register src="~/App/UserControls/HomePageVerticalCallout.ascx" tagName="Callout" tagPrefix="Home" %>
<%@ Reference Control="~/App/UserControls/LeftCallouts.ascx" %>
<%@Register src="~/App/UserControls/LeftCallouts.ascx" tagName="Callout" tagPrefix="Left" %>
<cms:CMSRepeater runat="server" ID="rptVerticalCallouts" ClassNames="Common.Callout">
<ItemTemplate><%if(HomePage){%><Home:Callout runat="server" runat="server"/><%}else if(LandingPage){%><Left:Callout LandingPage="True" runat="server"/><%}else if(InnerPage){%><Left:Callout InnerPage="True" runat="server" ID="leftCallout"/><%} %></ItemTemplate>
</cms:CMSRepeater>