﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Footer.ascx.cs" Inherits="CMSApp.App.UserControls.Footer" %>
<%@ Import Namespace="CMS.MacroEngine" %>
<%@ Import Namespace="CMS.DocumentEngine" %>
<%@ Import Namespace="CMS.Base" %>
<%@ Register Src="~/CMSWebParts/Newsletters/NewsletterSubscriptionWebPart.ascx" TagName="newsletterSignup" TagPrefix="uc1" %>

<div id="footer-modules">
    <asp:Repeater runat="server" ID="rptColumns">
        <ItemTemplate>
            <div id="footer-<%# Container.ItemIndex + 1%>">
                <div class="moduletable">
                    <ul class="mainlevel">
                        <li>
                            <a href="<%# Eval("Url") %>" class="mainlevel"><%# GetDisplayText(Eval("DisplayText") as string) %></a>
                            <ul>
                                <asp:Repeater runat="server" ID="rptColumnChildren" DataSource="<%# (Container.DataItem as MenuLink).Children != null ? (Container.DataItem as MenuLink).Children : new List<MenuLink>() %>">
                                    <ItemTemplate>
                                        <li>
                                            <a href="<%# Eval("Url") %>" class="sublevel"><%# Eval("DisplayText") %></a>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater><!-- Very quick and dirty way to remove the logo from this page. It should pick up the "Showfdic" field in the CMS, but I dont have time for that-->
    <% if (!(HttpContext.Current.Request.Url.AbsoluteUri.ToLower().Contains("community-financial-services-group-llc"))) 
       { %>
    <div id="footer-6">
        <div class="moduletable">
            <div class="custom">
                <p>
                    <a href="http://www.fdic.gov/" target="_blank" onclick="return SpeedbumpCtrl.navConfirm(this.href);">
                        <img src="/library/images/modules/cnb-fdic.png" alt="Member FDIC"></a>
                </p>
            </div>
        </div>
        <div class="subscribe">
            <uc1:newsletterSignup runat="server" ID="NewsletterSubscription" Visible="true" NewsletterName="CNBMarketing" DisplayFirstName="false" DisplayLastName="false" EmailText="Email" ButtonText="Submit" SendConfirmationEmail="true" />
        </div>
    </div>
    <% } %>
    
    <div style="display: block; clear: both; height: 0; font-size: 0px; color: #FFF; overflow: hidden"></div>
    <!-- CLEARING DIV -->

    <div class="social2">
        <a href="http://www.facebook.com/CommunityNationalBankVT" target="_blank" class="confirmed" onclick="return SpeedbumpCtrl.navConfirm(this.href);">
            <img src="/library/images/icons/social-icons-footer-new-facebook.png" alt="Facebook"></a>
        <a href="http://twitter.com/ComNatBankVT" target="_blank" class="confirmed">
            <img src="/library/images/icons/social-icons-footer-new-twitter.png" alt="Twitter" onclick="return SpeedbumpCtrl.navConfirm(this.href);"></a>
        <!--<a href="http://www.linkedin.com/company/community-national-bank" target="_blank" class="confirmed"><img src="/templates/cnb/images/social-icons-footer-new-linkedin.png" alt="Facebook"></a>-->
    </div>

    <div class="bottom-menu-2">
        <span class="lender">
            <img src="/library/images/bullet-green.png" width="14" height="20" alt="Need a Local Lender?"></span>
        <a href="/">Home</a> | <a href="/faq">FAQ's</a> | <a href="/locations">Locations</a> | <a href="/news">News</a> | <a href="/contact">Contact</a> | <a href="tel:+18023347915" style="white-space: nowrap;">802-334-7915</a>
    </div>
    <div style="display: block; clear: both; height: 0; font-size: 0px; color: #FFF; overflow: hidden"></div>
    <!-- CLEARING DIV -->

</div>
<!-- End: #footer-modules -->
