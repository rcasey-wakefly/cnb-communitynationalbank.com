﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Hero.ascx.cs" Inherits="CMSApp.App.UserControls.Hero" %>
<%@ Import Namespace="CMS.Base" %>

    <!-- CLEARING DIV -->


<cms:CMSRepeater runat="server" ID="rptHero" SelectTopN="1">
    <ItemTemplate>
        <div class="cnb-hero" style="background-image: url(<%=Image%>)">
            <div class="cnb-hero-inner grid-row">

                <div class="cnb-hero-info col lg-4 md-6">
                    <div class="col-inner">
                        <!-- This is left in a repeater as the button does not render if it is not in a repeater -->
                        <h3>
                            <%=Header %><span><%=SubHeader %></span>
                        </h3>
                        <p><%=Description %></p>
                        <div class="cnb-buttons">
                                <a href="<%=Link %>" class="cnb-button"><%=ButtonText %></a>
                        </div>
                    </div>
                </div>
                <div class="cnb-hero-netexp col lg-4 md-5 sm-12 fl">
                    <div class="col-inner">
                        <iframe src="/App/Templates/NetExpressLoginCallout.html" width="300" height="300"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </ItemTemplate>
</cms:CMSRepeater>


<div class="cnb-home-callouts grid-row">
    <cms:CMSRepeater runat="server" ID="rptCallouts" ClassNames="Common.Callout" Path="/Home/%" OrderBy="NodeOrder ASC" SelectTopN="4">
        <ItemTemplate>
            <div id="slideshow-module-<%#Container.ItemIndex + 1 %>" class="col lg-3 md-6 sm-12">
                <div id="slideshow-module-<%#Container.ItemIndex + 1 %>-inside" class="col-inner" >
                    <a href="<%#  Eval("Link") %>">
                        <div class="moduletable">
                            <h3><%# Eval("HeaderText") %></h3>
                            <%#Eval("Description") %>
                        </div>
                    </a>
                </div>
            </div>
        </ItemTemplate>
   </cms:CMSRepeater>
</div>
        