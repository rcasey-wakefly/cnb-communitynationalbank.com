﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LandingHero.ascx.cs" Inherits="CMSApp.App.UserControls.LandingHero" %>
<div class="landing-hero" runat="server" ID="innerHero" ClientIDMode="Static">
	<div class="landing-hero-header">
		<h1><%=HeroText %></h1>
	</div>
</div>