﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChildLinks.ascx.cs" Inherits="CMSApp.App.UserControls.ChildLinks" %>
<cms:CMSRepeater ID="rptChildLinks" runat="server" ClassNames="Common.Link" MaxRelativeLevel="1" OrderBy="NodeOrder">
	<ItemTemplate>
	    <li><a href="<%#Eval("Link") %>"><%#Eval("DisplayText") %></a></li>
	</ItemTemplate>
</cms:CMSRepeater>