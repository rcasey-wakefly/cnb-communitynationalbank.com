﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UtilityNav.ascx.cs" Inherits="App_UserControls_UtilityNav" %>
<!-- topmenu -->
<div id="topmenu">
    <div id="topmenu-inside">
        <div id="topmenu-module-1">
            <span class="lender">
                <img src="/library/images/bullet-gray.png" width="14" height="20" alt="Need a Local Lender?">
                Need a Local Lender?&nbsp;&nbsp;
            </span>
            <asp:Repeater runat="server" ID="rptFirstColumn">
                <ItemTemplate>
                    <%# Container.ItemIndex > 0 ? "|" : "" %>
                    <a class="<%#Eval("CssClass") %>" href="<%#Eval("Url") %>" <%# (Container.DataItem as MenuLink).LinkType == MenuLink.LinkTypes.ExternalLink ? "target='_blank'" : ""%>><%# Eval("DisplayText") %></a>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div id="topmenu-module-2">
            <span class="lender">
                <img src="/library/images/bullet-green.png" width="14" height="20" alt="Need a Local Lender?"></span>
            <asp:Repeater runat="server" ID="rptSecondColumn">
                <ItemTemplate>
                    <a class="<%#Eval("CssClass") %>" href="<%#Eval("Url") %>" <%# (Container.DataItem as MenuLink).LinkType == MenuLink.LinkTypes.ExternalLink ? "target='_blank'" : ""%>><%# Container.ItemIndex > 0 ? "| " : "" %><%# Eval("DisplayText") %></a>
                </ItemTemplate>
                <FooterTemplate>
                   <a href="tel:+18023347915">| 802-334-7915</a>
                </FooterTemplate>
            </asp:Repeater>

            <div class="social">
                <a href="http://www.facebook.com/CommunityNationalBankVT" target="_blank" class="confirmed" onclick="return SpeedbumpCtrl.navConfirm(this.href);">
                    <img src="/library/images/social-icons-new-facebook.png" alt="Facebook"></a>
                <a href="http://twitter.com/ComNatBankVT" target="_blank" class="confirmed" onclick="return SpeedbumpCtrl.navConfirm(this.href);">
                    <img src="/library/images/social-icons-new-twitter.png" alt="Facebook"></a>
            </div>
        </div>
    </div>
</div>
