﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchResults.ascx.cs" Inherits="CMSApp.App.UserControls.SearchResults" %>
<%@ Register src="~/CMSModules/SmartSearch/Controls/SearchResults.ascx" tagname="SearchResults" tagprefix="uc" %>

<h1>Search Results</h1>
<p>You searched for "<% = QueryHelper.GetString("SearchText","") %>"</p>
<uc:SearchResults ID="searchResults" runat="server" 
    Indexes="NHHSSimplePagesSearch" 
    SearchSort="##SCORE##" 
    ContentLoaded="true" 
    NoResultsText="No results found!"
    HidePagerForSinglePage="false" 
    TransformationName="CMS.Root.SmartSearchResults" 
    DisplayFirstLastAutomatically="true" 
    DisplayPreviousNextAutomatically="true" 
    MaxResults="100" 
    GroupSize="10"
    PagesTemplateName="CMS.PagerTransformations.General-Pages" 
    CurrentPageTemplateName="CMS.PagerTransformations.General-CurrentPage"
    FirstPageTemplateName="cms.pagertransformations.General-FirstPage" 
    LastPageTemplateName="CMS.PagerTransformations.General-LastPage"
    PreviousGroupTemplateName="CMS.PagerTransformations.General-PreviousGroup"
    NextGroupTemplateName="CMS.PagerTransformations.General-NextGroup"
    PreviousPageTemplateName="CMS.PagerTransformations.General-PreviousPage"
    NextPageTemplateName="CMS.PagerTransformations.General-NextPage"
    SeparatorTemplateName="CMS.PagerTransformations.General-PageSeparator"
    LayoutTemplateName="CMS.PagerTransformations.General-PagerLayout" />