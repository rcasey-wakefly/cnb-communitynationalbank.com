﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LeftNav.ascx.cs" Inherits="CMSApp.App.UserControls.LeftNav" %>
<%@ Register Src="~/App/UserControls/Callout.ascx" TagName="Out" TagPrefix="Call" %>



<div class="tree">
    <div class="start"></div>
    <ul>
        <li class="mainlevel_active">
            <ul>
                <%foreach (var Link in FirstLevelLinks)
                { %>
                    <li class="<%= Link.ShowChildren ? "sublevel_current" : "sublevel" %>">
                        <a href="<%= Link.Node.NodeAliasPath %>" <%= Link.ShowChildren ? "class='sublevel_current' id='active_menu'" : "class='sublevel'" %> ><%= Link.DocumentName %></a>
                        <% if (Link.ShowChildren && Link.Children.Any())
                        {%>
                            <ul>
                                <%foreach (var child in Link.Children)
                                  {%>
                                    <%=DisplayTree(child)%>
                                <%}%>
                            </ul>
                        <% } %>
                    </li>
                <% } %>
            </ul>
        </li>
    </ul>
</div>
