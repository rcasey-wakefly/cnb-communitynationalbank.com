﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMSApp.App.UserControls
{
    public partial class Callout : System.Web.UI.UserControl
    {
        public string NodeAliasPath
        {
            get { return nodeAliasPath; }
            set
            {
                nodeAliasPath = value + "/%";

                rptVerticalCallouts.Path = nodeAliasPath;
                rptVerticalCallouts.ReloadData(true);
            }
        }

        public bool LandingPage { get; set; }
        public bool InnerPage { get; set; }

        public bool HomePage { get; set; }
        private string nodeAliasPath { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }
    }
}