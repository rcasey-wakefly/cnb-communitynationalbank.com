﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LinkColumn.ascx.cs" Inherits="CMSApp.App.UserControls.LinkColumn" %>
<%@ Register src="~/App/UserControls/ParentLinks.ascx" tagName="Parent" tagPrefix="Links" %>
<cms:CMSRepeater runat="server" ID="rptLinkColumns" ClassNames="Common.LinkColumn" MaxRelativeLevel="1" OrderBy="NodeOrder">
	<ItemTemplate><%----%><div class="col-5">
	        <ul>
	            <Links:Parent runat="server" NodeAliasPath=<%#Eval("NodeAliasPath") %>/> 
            </ul>
        </div><%----%></ItemTemplate>
</cms:CMSRepeater>