﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Amazon.DynamoDB.DocumentModel;
using CMS.Base;
using CMS.DocumentEngine;
using CMSApp.App.Classes.Helpers;
using LinqToTwitter;
using Microsoft.Ajax.Utilities;

namespace CMSApp.App.UserControls
{
    public partial class Hero : BaseHero
    {
        /*
        public List<TreeNode> BelowHeroCallouts { get; set; }
        public int CalloutCount { get; set; }
        */
        protected void Page_Load(object sender, EventArgs e)
        {
            if (CurrentDocument == null || IsPostBack) return;
            /*
            BelowHeroCallouts = new List<TreeNode>();
            BelowHeroCallouts = CurrentDocument.GetChildren("Common.Callout", 1, "HeaderText", "Description", "Link").ToList();
                //DocumentHelper.GetDocuments("Common.Callout").Path(CurrentDocument.NodeAliasPath, PathTypeEnum.Children).Columns("HeaderText", "Description").ToList();
            CalloutCount = BelowHeroCallouts.Count();

            rptCallouts.DataSource = BelowHeroCallouts.Select(node => new
            {
                HeaderText = node.GetStringValue("HeaderText", string.Empty),
                Description = node.GetStringValue("Description", string.Empty),
                Link = node.GetStringValue("Link", string.Empty),
            });

            rptCallouts.DataBind();
            */
        }
    }
}