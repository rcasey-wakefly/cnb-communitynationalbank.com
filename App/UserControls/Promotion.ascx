﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Promotion.ascx.cs" Inherits="App_UserControls_Promotion" %>
<%@ Import Namespace="CMS.Base" %>

<ul class="cnb-home-content home-callouts-row grid-row">
    <cms:CMSRepeater runat="server" ID="rptPromotions" ClassNames="Common.Promotion" Path="/Promotions/%" OrderBy="NodeOrder ASC" SelectTopN="4">
        <ItemTemplate>
            <li class="col lg-3 md-6 sm-12">
                <div class="col-inner">
                    <a href="<%# Eval("PromotionURL") %>" class="cnb-home-community">
                        <h2><%# Eval("PromotionHeading") %></h2>
                        <p><%# Eval("PromotionSummary") %></p>
                    </a>
                </div>
            </li>
        </ItemTemplate>
    </cms:CMSRepeater>
</ul>