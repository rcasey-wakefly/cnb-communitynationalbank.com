﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Header.ascx.cs" Inherits="CMSApp.App.UserControls.Header" %>
<%@ Import Namespace="CMSApp.App.Classes.Helpers" %>
<%--<%@ Register Src="/App/UserControls/UserNav.ascx" TagName="Nav" TagPrefix="User" %>
<%@ Register Src="/App/UserControls/MobileUserNav.ascx" TagName="Nav" TagPrefix="MobileUser" %>--%>
<%@ Register Src="/App/UserControls/HeaderSearch.ascx" TagName="Search" TagPrefix="Header" %>
<%@ Register Src="/App/UserControls/HeaderMobileSearch.ascx" TagName="MobileSearch" TagPrefix="MobileHeader" %>

<header>
    <% if (ShowTopBar)
       {%>
        <div class="header-top-bar">
            <div class="top-nav-buttons">
                <a class="button top-blue-button" href="/join">Join &amp; Support</a>
                <a class="button top-blue-button minimize" href="/join/donate/donate-now">Donate<span> Now</span></a>
                <a class="button top-blue-button minimize" href="/newsletter"><span>Sign up for our </span>ENewsletter</a>
                <a class="button top-blue-button" href="/contact-us">Contact Us</a>
                <a class="button top-blue-button" href="/store">Store</a>
                <a class="shopping-cart" href="<%= ResolveUrl("~/Store/Checkout/Cart") %>"><i class="fa fa-shopping-cart"></i>My Cart <strong>(<%= CartCount %>)</strong></a>
            </div>
            <Header:Search runat="server" Placeholder="Search NH Historical Society" />
            <div style="clear: both;"></div>
        </div>
    <%}%>


    <div class="header-red-bar <%=CssClass %>">
        <div class="logo">
            <a href="/">
                <img src="<%=HeaderImage %>" alt=""></a>
        </div>
        <div class="mobile-toggles">
            <% if(ShowTopBar)
               { %>
                <div class="account">
                    <a href="#" class="account-box">
                        <i class="fa fa-user"></i>
                        <span>Account</span>
                    </a>
                </div>
           <% } %>
            
            <div class="menu-toggle">
                <a href="#">
                    <input type="checkbox" id="toggle-nav" name="menu-close">
                    <label for="toggle-nav">
                        <span></span>
                        <span></span>
                        <span></span>
                    </label>
                    <span id="menu-close">Menu</span>
                </a>
            </div>
        </div>
    </div>
    <% if (ShowTopBar)
    {%>
<%--        <div id="mobileUserNav">
            <MobileUser:Nav runat="server" />
        </div>--%>
     <%} %>
    <% if (!ShowTopBar)
       {%>
        <Header:Search runat="server" Placeholder="Search NH History Network" />

        <div class="network-header-buttons">
            <a href="<%= GlobalSettingsHelper.GetSettingsKeyStringValue("SocietySiteUrl", string.Empty) %>" class="historical-society">N<span>ew </span>H<span>ampshire </span>H<span>istorical </span>S<span>ociety</span></a>
            <a href="<%= GlobalSettingsHelper.GetSettingsKeyStringValue("FacebookUrl", string.Empty) %>" class="facebook"></a>
        </div>
<%--        <User:Nav runat="server" />--%>
    <%}%>
        <MobileHeader:MobileSearch runat="server" ID="mobileSearch" Placeholder="Search NH History Network" />
    <div style="clear: both;"></div>
</header>
