﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuLink.ascx.cs" Inherits="App_UserControls_MegaMenu_MenuLink" %>

<li class="mega submenu-align-auto <%= IsActive ? "active" : "" %> <%= IsFirst ? "first" : IsLast ? "last" : string.Empty %> <%= MenuLink.CssClass %>">
    <a href="<%= MenuLink.Url %>" class="mega <%= IsActive ? "active" : "" %>" <%= MenuLink.LinkType == MenuLink.LinkTypes.ExternalLink ? "target='_blank'" : "" %>><span class="menu-title"><%= MenuLink.DisplayText %></span></a>
</li>
