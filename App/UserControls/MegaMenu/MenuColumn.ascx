﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuColumn.ascx.cs" Inherits="App_UserControls_MegaMenu_MenuColumn" %>

<li class="mega submenu-align-auto <%= IsFirst ? "first" : IsLast ? "last" : string.Empty %> <%= MenuLink.Children.Any() ? "haschild" : "" %> <%= IsActive ? "active" : "" %>">
    <a href="<%= MenuLink.Url %>" class="mega <%= IsFirst ? "first" : IsLast ? "last" : string.Empty %> <%= IsActive ? "active" : "" %>">
        <asp:Literal runat="server" ID="litDisplayText"></asp:Literal></a>
    <div id="childContent" runat="server">
        <div class="childcontent-inner-wrap">
            <div class="childcontent-inner clearfix">
                <%if (!IsImageChild)
                  {%>
                <div id="divColumn" runat="server" style="width: 179px;">
                    <ul class="megamenu level1">
                        <asp:PlaceHolder runat="server" ID="plcChild"></asp:PlaceHolder>
                    </ul>
                </div>
                <% }
                  else
                  {%>
                <asp:PlaceHolder runat="server" ID="plcImageChild"></asp:PlaceHolder>
                <% }%>
            </div>
        </div>
    </div>
</li>


