﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.DocumentEngine;
using CMS.SiteProvider;
using MenuLayouts.MenuLayouts.Services;

public partial class App_UserControls_MegaMenu_MobileMenu : System.Web.UI.UserControl
{
    public static int CacheMinutes { get { return GlobalSettingsHelper.GetSettingsKeyIntegerValue("MegaMenuCacheMinutes"); } }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected override void OnInit(EventArgs e)
    {
        var menu = new Menu();

        if (!menu.IsEmpty)
        {
            var cacheKey = "mobile_menu_" + SiteContext.CurrentSiteName;
            var items = MenuCacheService.Get(cacheKey) as List<ListItem>;

            if (items == null)
            {
                items = new List<ListItem>();
                foreach (var column in menu.Columns)
                {
                    var item = new ListItem("- " + column.DisplayText, column.Url);

                    items.Add(item);

                    if(column.Children.Any())
                    {
                        items.AddRange(GetChildrenListItems(column, "&nbsp;&nbsp;"));
                    }
                }

                // store in cache
                // TODO: Add this setting to the MenuLayouts module
                var cacheMin = CacheMinutes;

                cacheMin = cacheMin > 0 ? cacheMin : 60;

                //CacheService.Add(string.Format("mobile_menu_" + SiteContext.CurrentSiteName), items, DateTimeOffset.Now.AddHours(1));
                MenuCacheService.Add(cacheKey, items, DateTime.Now.AddMinutes(cacheMin), TimeSpan.Zero);
            }

            ddlMenu.Items.AddRange(items.ToArray());
        }

        base.OnInit(e);
    }

    protected void ddlMenu_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Response.Redirect(ddlMenu.SelectedValue);
    }

    private IEnumerable<ListItem> GetChildrenListItems(MenuLink currentLink, string indent)
    {
        var items = new List<ListItem>();

        foreach (var child in currentLink.Children)
        {
            var decodedIndent = HttpUtility.HtmlDecode(indent);
            var item = new ListItem(string.Format("{0}- {1}", decodedIndent, child.DisplayText), child.Url);

            item.Selected = child.NodeAliasPath.Equals(DocumentContext.CurrentDocument.NodeAliasPath);

            if(child.LinkType == MenuLink.LinkTypes.Header)
            {
                item.Attributes.Add("disabled", "disabled");
            }

            items.Add(item);

            if (child.Children.Any())
            {
                var newIndent = indent + "&nbsp;&nbsp;";
                items.AddRange(GetChildrenListItems(child, newIndent));
            }
        }

        return items;
    }
}