﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuImageLink.ascx.cs" Inherits="App_UserControls_MegaMenu_MenuImageLink" %>
<div class="megacol column <%= IsFirst ? "first" : IsLast ? "last" : string.Empty %> " style="width: 179px;">
    <ul class="megamenu level1">
        <li class="mega first group <%= IsActive ? "active" : "" %> submenu-align-auto">
            <div class="group clearfix">
                <div class="group-title <%= IsActive ? "active" : "" %>">
                    <a href="<%= MenuLink.Url %>" class="menu144 mega first group <%= IsActive ? "active" : "" %>">
                        <img src="<%= MenuLink.ImageUrl %>" alt="<%= MenuLink.DisplayText %>"><span class="image-title"><%= MenuLink.DisplayText %></span> </a>
                </div>
                <div class="group-content">
                    <ul class="megamenu level1">
                        <asp:PlaceHolder runat="server" ID="plcChildren"></asp:PlaceHolder>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</div>
