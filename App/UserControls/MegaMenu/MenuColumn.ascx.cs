﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.DocumentEngine;

public partial class App_UserControls_MegaMenu_MenuColumn : BaseMenuControl
{
    public bool IsImageChild { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        SetDisplayText();
        childContent.Visible = MenuLink.Children != null ? MenuLink.Children.Any() : false;

        if(childContent.Visible)
        {
            childContent.Attributes.Add("class", string.Format("childcontent cols{0}", MenuLink.Children.Count()));
            SetChildrenControls();
        }
    }

    private void SetDisplayText()
    {
        var displayTextArray = MenuLink.DisplayText.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

        var firstWord = displayTextArray[0];

        var remainingText = string.Empty;

        if (displayTextArray.Length > 1)
        {
            for (int i = 1; i < displayTextArray.Length; i++)
            {
                remainingText += " " + displayTextArray[i];
            }
        }

        var strBuilder = new StringBuilder();
        strBuilder.Append(string.Format("<span class='menu-title'>{0}", firstWord));
        strBuilder.Append(string.Format("<span class='other'>{0}</span></span><span class='menu-desc'>{0}</span>", remainingText));

        litDisplayText.Text = strBuilder.ToString();
    }

    protected void SetChildrenControls()
    {
        foreach(var child in MenuLink.Children)
        {
            if (!string.IsNullOrEmpty(child.ControlPath))
            {
                var linkControl = LoadControl(child.ControlPath) as BaseMenuControl;
                var isFirstChild = child == MenuLink.Children.First();
                var isLastChild = child == MenuLink.Children.Last();

                IsImageChild = child.LinkType == MenuLink.LinkTypes.Image;

                if (linkControl != null)
                {
                    linkControl.MenuLink = child;
                    linkControl.IsFirst = IsFirst;
                    linkControl.IsLast = IsLast;

                    if (!IsImageChild)
                    {
                        divColumn.Attributes.Add("class", string.Format("megacol column {0}", isFirstChild ? "first" : isLastChild ? "last" : string.Empty));

                        plcChild.Controls.Add(linkControl);
                    }
                    else
                    {
                        plcImageChild.Controls.Add(linkControl);
                    }
                }
            }
        }
    }
}