﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class App_UserControls_MegaMenu_MenuImageLink : BaseMenuControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected override void OnInit(EventArgs e)
    {
        SetChildrenControls();

        base.OnInit(e);
    }

    protected void SetChildrenControls()
    {
        foreach (var child in MenuLink.Children)
        {
            if (!string.IsNullOrEmpty(child.ControlPath))
            {
                var linkControl = LoadControl(child.ControlPath) as BaseMenuControl;

                if (linkControl != null)
                {
                    linkControl.MenuLink = child;
                    linkControl.IsFirst = child == MenuLink.Children.First();
                    linkControl.IsLast = child == MenuLink.Children.Last();

                    plcChildren.Controls.Add(linkControl);
                }
            }
        }
    }
}