﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuHeader.ascx.cs" Inherits="App_UserControls_MegaMenu_MenuHeader" %>
<li class="mega <%= IsFirst ? "first" : IsLast ? "last" : string.Empty %> <%= MenuLink.CssClass %> submenu-align-auto">
    <span class="mega <%= IsFirst ? "first" : IsLast ? "last" : string.Empty %> <%= MenuLink.CssClass %> ">
        <span class="menu-title"><%= MenuLink.DisplayText %></span>
    </span>
</li>