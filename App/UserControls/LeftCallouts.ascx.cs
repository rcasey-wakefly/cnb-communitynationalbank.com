﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMSApp.App.UserControls
{
    public partial class LeftCallouts : System.Web.UI.UserControl
    {
        public bool LandingPage { get; set; }
        public bool InnerPage { get; set; }
        public string CssClass { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (LandingPage) CssClass = "landing-sidebar-block";
            if (InnerPage) CssClass = "inner-sidebar-cta-block";
        }
    }
}