﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.DocumentEngine;
using CMS.Helpers;
using CMS.SiteProvider;
using CMSApp.App.Classes;

namespace CMSApp.App.UserControls
{
    public partial class EmailThisCtrl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                shareSubject.Text = string.Format("{0} - {1}", DocumentContext.CurrentDocument.DocumentName, SiteContext.CurrentSite.DisplayName);
            }
        }

        protected void sendEmail_Click(object sender, EventArgs e)
        {
            lblError.Visible = false;
            var body = string.Format("{0} has shared the following item with you. <br /><br />", shareFrom.Text.Trim());
            body += shareBody.Text.Replace("\n", "<br /><br />").Trim();

            if(EmailThis.ShareEmail(shareTo.Text.Trim(), shareFrom.Text.Trim(), shareSubject.Text.Trim(), body))
            {
                ScriptManager.RegisterClientScriptBlock(Page, GetType(), Guid.NewGuid().ToString(), "EmailThisCtrl.emailSent();", true);
            }
            else
            {
                lblError.Visible = true;
            }
        }

        public string GetAbsoluteUrl(string relativeUrl)
        {
            var absUrl = URLHelper.GetAbsoluteUrl(relativeUrl);

            return absUrl;
        }
    }
}