﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmailThisCtrl.ascx.cs" Inherits="CMSApp.App.UserControls.EmailThisCtrl" %>
<%@ Import Namespace="CMS.DocumentEngine" %>
<%@ Import Namespace="CMS.SiteProvider" %>

<div id="modal-email-share" class="modal-content content-section highlight">
    <div id="emailHasSent">
        <h2>Your E-mail has been sent.</h2>

        <a onclick="return EmailThisCtrl.closeEmail();" class="button highlight"><i class="fa fa-close"></i> Close</a>
    </div>
    <div id="emailNotSent">
        <asp:UpdatePanel runat="server" ID="upEmail">
            <ContentTemplate>

                <h2>Share via E-mail</h2>
                <p>
                    Enter the E-mail Address that you'd like to send this page to.
                </p>
                <div class="form">
                    <asp:Label ID="lblError" runat="server" Text="Failed to send email, please try again." Visible="False"/>
                    <div class="form-item">
                        <label for="shareTo">E-mail to:</label>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="shareTo" Display="Dynamic" ValidationGroup="email" ErrorMessage="This field is required." CssClass="email-error"/>
                            <asp:RegularExpressionValidator runat="server" ControlToValidate="shareTo" Display="Dynamic" ValidationGroup="email" CssClass="email-error"
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Email must be in proper format. EG: me@company.com" />
                        <asp:TextBox runat="server" ID="shareTo" ClientIDMode="Static"/>
                        
                        <label for="shareFrom">E-Mail from:</label>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="shareFrom" Display="Dynamic" ValidationGroup="email" ErrorMessage="This field is required." CssClass="email-error"/>
                            <asp:RegularExpressionValidator runat="server" ControlToValidate="shareFrom" Display="Dynamic" ValidationGroup="email" CssClass="email-error"
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Email must be in proper format. EG: me@company.com" />
                        <asp:TextBox runat="server" ID="shareFrom" ClientIDMode="Static"/>
                        
                        <label for="shareSubject">Subject:</label>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="shareSubject" Display="Dynamic" ValidationGroup="email" ErrorMessage="This field is required." CssClass="email-error"/>
                        <asp:TextBox runat="server" ID="shareSubject" ClientIDMode="Static"/>
                        
                        <label for="shareBody">Body:</label>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="shareBody" Display="Dynamic" ValidationGroup="email" ErrorMessage="This field is required." CssClass="email-error"/>
                        <asp:TextBox runat="server" ID="shareBody" Rows="8" TextMode="MultiLine" ClientIDMode="Static" CssClass="email-text-area"/>
                        
                    </div>
                    <p>
                        <a onclick="return EmailThisCtrl.closeEmail(event);" class="button highlight blue-button"><i class="fa fa-close"></i> Cancel</a>
                        <asp:LinkButton runat="server" ID="sendEmail" OnClick="sendEmail_Click" class="button blue-button highlight"><i class="fa fa-envelope"></i> Send</asp:LinkButton>
                    </p>
                </div>
            </ContentTemplate>

             <Triggers>
                <asp:AsyncPostBackTrigger ControlID="sendEmail" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</div>
