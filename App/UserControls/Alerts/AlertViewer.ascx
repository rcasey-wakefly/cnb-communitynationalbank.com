﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlertViewer.ascx.cs" Inherits="App_UserControls_Alerts_AlertViewer"%>


<cms:CMSRepeater runat="server" ID="rptAlert">
    <ItemTemplate>
        <div class="cnb-alert">
            <div class="cnb-alert-inner">
                <%# Eval("AlertDescription") %>
                <div class="close-alert"></div>
             </div>
        </div>
    </ItemTemplate>
</cms:CMSRepeater>

