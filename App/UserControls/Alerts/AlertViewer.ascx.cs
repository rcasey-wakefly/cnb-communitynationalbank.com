﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Alerts;
using CMS.Controls;
using CMS.Helpers;
using CMS.PortalControls;
using CMS.Base;

public partial class App_UserControls_Alerts_AlertViewer : CMSAbstractWebPart
{
    public AlertInfo alertToShow = new AlertInfo();
    //Called when the web part is loaded on the page
    protected override void OnInit(EventArgs e)
    {
        var activeAlerts = new List<AlertInfo>();
        var alerts = AlertInfoProvider.GetAlerts().ToArray();

        //Add only alerts that are active to the list
        foreach (var alert in alerts)
        {
            if (alert.AlertActive)
            {
                if (alert.AlertScheduled)
                {
                    DateTime currentTime = DateTime.Now;
                    if (currentTime > alert.AlertStartTime && currentTime < alert.AlertStopTime)
                    {
                        activeAlerts.Add(alert);
                    }
                }
                else
                {
                    activeAlerts.Add(alert);
                }
            }
        }
        if (activeAlerts.Count == 0)
        {
            alertToShow.AlertDescription = "";
            rptAlert.Visible = false;
        }
        else
        {
            alertToShow.AlertOrder = Int32.MaxValue;
            foreach (var alert in activeAlerts)
            {
                if (alert.AlertOrder < alertToShow.AlertOrder)
                {
                    alertToShow = alert;
                }
            }
        }
        rptAlert.DataSource = alertToShow;
        rptAlert.DataBind();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //Required by the inherited class
    }
}