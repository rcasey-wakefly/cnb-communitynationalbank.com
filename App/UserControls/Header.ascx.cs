﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.Ecommerce;
using CMS.Helpers;
using CMS.SiteProvider;

namespace CMSApp.App.UserControls
{
    public partial class Header : System.Web.UI.UserControl
    {
        public bool ShowTopBar { get; set; }
        public string CssClass { get; set; }
        public int CartCount { get; set; }
        public string HeaderImage { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ECommerceContext.CurrentShoppingCart == null)
            {
                CartCount = 0;
            }
            else
            {
                //CartItems is a count that includes product options, if a shirt has a color and a size, it is represented by 3 cart items, not 1 unit
                CartCount = ECommerceContext.CurrentShoppingCart.TotalUnits;
            }

            mobileSearch.Placeholder = SiteContext.CurrentSiteName.Equals("NHHS", StringComparison.OrdinalIgnoreCase) 
                ? "Search NH Historical Society" 
                : "Search NH History Network";
        }

       
    }
}