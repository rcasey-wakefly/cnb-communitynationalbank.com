﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App.Helpers;
using CMS.Membership;
using Microsoft.Ajax.Utilities;

public partial class App_UserControls_FormQuickLinkRepeater : System.Web.UI.UserControl
{
    protected override void OnInit(EventArgs e)
    {
        var forms = CustomTableHelper.GetItems("customtable.formpages").OrderBy(x => x.GetIntegerValue("ItemOrder", Int32.MaxValue)).Select(x => new ListItem() { Value = x.GetStringValue("FormLink", "/"), Text = x.GetStringValue("FormName", "Form Issue") }).ToArray();
        rptQuickLinks.DataSource = forms;
        rptQuickLinks.DataBind();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            //New code here
        }
    }
}