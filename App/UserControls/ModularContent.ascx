﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ModularContent.ascx.cs" Inherits="App_UserControls_ModularContent" %>
<%@Register src="~/App/UserControls/FormDropDownList.ascx" tagName="dropdown" tagPrefix="form" %>
<%@ Register src="~/CMSModules/PortalEngine/Controls/Editable/EditableText.ascx" tagname="EditableText" tagprefix="cms" %>
<%@ Register Src="~/App/UserControls/FormQuickLinkRepeater.ascx" TagName="Repeater" TagPrefix="QuickLinks" %>


<div class="cnb-home-content grid-row cnb-home-toplevel">

    <div class="col lg-6 sm-12">
        <div class="col-inner cnb-home-news">
            <h1 class="cnb-header">Recent Announcements</h1>
            <cms:CMSPagePlaceholder runat="server" ClientIDMode="Static" >
                <LayoutTemplate>
                    <cms:CMSWebPartZone runat="server" ID="ModularContent1"/>
                </LayoutTemplate>
            </cms:CMSPagePlaceholder>
            <cms:CMSPagePlaceHolder runat="server" ClientIDMode="Static">
                <LayoutTemplate>
                    <cms:CMSWebPartZone runat="server" ID="RecentAnnouncmentsModularContent" />
                </LayoutTemplate>
            </cms:CMSPagePlaceHolder>
        </div>
    </div>

        <!-- Repeater to display QuickLinks (References FormQuickLinkRepeater.ascx) -->
    <div>
        <QuickLinks:Repeater runat="server" ID="rptQuickLinks"/>
    </div>

    <!-- These are the quick links hardcoded -->

    <!--
    <div class="col lg-6 sm-12">
        <div class="col-inner cnb-home-quicklinks">
            <h1 class="cnb-header">Quick Links</h1>
            <div class="cnb-home-signup">
                <h1>
                    Sign up TODAY for <br />
                    <span>NetExpress Online Bill Pay</span>
                </h1>
                <div>
                    <a href="#" class="cnb-button">Open Account</a>
                </div>
            </div>

            <ul class="cnb-links">
                <li><a href="#">Donation Request Form</a></li>
                <li><a href="#">NetExpress Online Bill Pay</a></li>
                <li><a href="#">Community Circle</a></li>
                <li><a href="#">Community Service Award Nomination</a></li>
                <li><a href="#">Overdraft Protection</a></li>
                <li><a href="#">Careers</a></li>
            </ul>

        </div>
    </div>
    -->

</div>

<!-- Promotions Repeater -->
<div>
    <ul class="cnb-home-content home-callouts-row grid-row">
        <cms:CMSRepeater runat="server" ID="rptPromotions" ClassNames="Common.Callout" Path="/Promotions/%" OrderBy="NodeOrder ASC" SelectTopN="4">
            <ItemTemplate>
                <li class="col lg-3 md-6 sm-12">
                    <div class="col-inner">
                        <a href="<%# Eval("Link") %>" class="cnb-home-community">
                            <h2><%# Eval("HeaderText") %></h2>
                            <p><%# Eval("Description") %></p>
                        </a>
                    </div>
                </li>
        </ItemTemplate>
        </cms:CMSRepeater>
    </ul>
</div>


<!-- Hardcoded Promotions (for reference) -->

<!--
<ul class="cnb-home-content home-callouts-row grid-row">

    <li class="col lg-3 md-6 sm-12">
        <div class="col-inner">
            <a href="#" class="cnb-home-community">
                <h2>Community <span>Circle</span></h2>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam tincidunt mauris eu risus.</p>
            </a>
        </div>
    </li>

    <li class="col lg-3 md-6 sm-12">
        <div class="col-inner">
            <a href="#" class="cnb-home-community">
                <h2>Totally Kids <span>Club</span></h2>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam tincidunt mauris eu risus.</p>
            </a>
        </div>
    </li>

    <li class="col lg-3 md-6 sm-12">
        <div class="col-inner">
            <a href="#" class="cnb-home-community">
                <h2>Promotional <span>Area</span></h2>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam tincidunt mauris eu risus.</p>
            </a>
        </div>
    </li>

    <li class="col lg-3 md-6 sm-12">
        <div class="col-inner">
            <a href="#" class="cnb-home-community">
                <h2>Promotional <span>Area</span></h2>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam tincidunt mauris eu risus.</p>
            </a>
        </div>
    </li>

</ul>       
-->

<div class="cnb-grey-block">
    <div class="grid-row bottom-level-row">

        <div class="col lg-6 sm-12">
            <div class="col-inner cnb-important-notices">
                <cms:CMSPagePlaceholder runat="server" ClientIDMode="Static" >
                    <LayoutTemplate>
                        <cms:CMSWebPartZone runat="server" ID="ModularContent5"/>
                    </LayoutTemplate>
               </cms:CMSPagePlaceholder>
            </div>
        </div>

        <div class="col lg-6 sm-12">
            <div class="col-inner cnb-security-tips">
                <cms:CMSPagePlaceholder runat="server" ClientIDMode="Static" >
                    <LayoutTemplate>
                        <cms:CMSWebPartZone runat="server" ID="ModularContent6"/>
                    </LayoutTemplate>
                </cms:CMSPagePlaceholder>

            </div>
        </div>

    </div>

    <ul class="cnb-mobile-apps">
        <li style="display: block;"><h2>Download our app!</h2></li>
        <li><a href="https://appsto.re/us/eXqH5.i" target="_blank" onClick="return SpeedbumpCtrl.navConfirm(this.href);"><img src="/library/images/icons/Available-on-AppStore.png" alt="Available on the App Store." /></a></li>
        <li><a href="https://play.google.com/store/apps/details?id=com.fi6143.godough" target="_blank" onClick="return SpeedbumpCtrl.navConfirm(this.href);"><img src="/library/images/icons/Google_Play.png" alt="Available on Google Play." /></a></li>
    </ul>

</div>