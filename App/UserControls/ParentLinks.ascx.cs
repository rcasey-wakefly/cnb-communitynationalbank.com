﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMSApp.App.UserControls
{
    public partial class ParentLinks : System.Web.UI.UserControl
    {
        public string NodeAliasPath
        {
            get { return nodeAliasPath; }
            set
            {
                nodeAliasPath = value + "/%";
                rptParentLinks.Path = nodeAliasPath;
                rptParentLinks.ReloadData(true);
            }
        }

        private string nodeAliasPath { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}