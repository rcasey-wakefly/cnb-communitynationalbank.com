﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ParentLinks.ascx.cs" Inherits="CMSApp.App.UserControls.ParentLinks" %>
<%@Register src="~/App/UserControls/ChildLinks.ascx" tagName="Child" tagPrefix="Links" %>
<cms:CMSRepeater ID="rptParentLinks" runat="server" ClassNames="Common.Link" MaxRelativeLevel="1" OrderBy="NodeOrder">
	<ItemTemplate>
	    <li><a href="<%#Eval("Link") %>" class="heading"><%#Eval("DisplayText") %></a></li>
	    <Links:Child runat="server" NodeAliasPath=<%#Eval("NodeAliasPath") %>/>
    </ItemTemplate>
</cms:CMSRepeater>