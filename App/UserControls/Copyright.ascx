﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Copyright.ascx.cs" Inherits="CMSApp.App.UserControls.Copyright" %>

<div style="clear: both; padding-top: 10px;">
</div>

<p style="text-align: center;" class="copyrightjam">
    &copy; <%= DateTime.Now.Year %> <a href="/">Community National Bank</a><!--, All rights reserved.-->

    | <a href="/sitemap">Sitemap</a>
    | <a href="/contact">Contact</a>
    | <a href="/phone-numbers">Phone Numbers</a>
    | <a href="/security-statement">Security</a>
    | <a href="/privacy" target="_blank">Privacy</a>
    | <a href="/web-site-privacy">Website Privacy</a>
    | <a href="/applications">Applications</a>
    | <a href="/disclosures">Disclosure</a>
    | <a href="/childrens-privacy">Online Children's Privacy</a>

</p>
<br>
<p style="text-align: center;">Terms and conditions of accounts, products and services are subject to change. </p>
<br>
