﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HeaderSearch.ascx.cs" Inherits="CMSApp.App.UserControls.HeaderSearch" %>
<div class="top-search-block">
	<div class="search-row">
		<div class="search-field">
		    <label for="txtSearch" class="visuallyhidden">Search</label>
			<input type="search" id="txtSearch" class="header-txt-search" runat="server" ClientIDMode="Static"/>
		</div>
		<div class="search-button"> 
			<button id="btnSearch" class="header-btn-search" runat="server" OnServerClick="btnSearch_OnServerClick">
				<i class="fa fa-search"></i><span class="visuallyhidden">Submit Search</span>
			</button>
		</div>
	</div>
</div>