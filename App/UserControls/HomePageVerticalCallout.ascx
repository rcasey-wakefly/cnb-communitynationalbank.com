﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HomePageVerticalCallout.ascx.cs" Inherits="CMSApp.App.UserControls.HomePageVerticalCallout" %>
<div class="home-vertical-callout">
	<a href="<%#Eval("Link") %>"><%#renderImage(Eval("Image").ToString(), Eval("ImageAltText").ToString()) %></a>
	<h2 class="center-headline"><a href="<%#Eval("Link") %>"><%#Eval("HeaderText") %></a></h2>
	<p><%#Eval("Description") %></p>
</div><!-- -->