﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MegaMenu.ascx.cs" Inherits="App_UserControls_MegaMenu" %>

<header class="header">
    <div class="header-inner clearfix">
        <a class="brand pull-left" href="/Home">
            <img src="/library/images/logo.png" alt="Community National Bank" class="no-rightclick">
            <script type="text/javascript">
                $(".no-rightclick").bind("contextmenu", function(e){
                    return false;
                });
            </script>
        </a>
        <div class="header-search pull-right">
            <!--<div class="phone"></div>-->
            <div id="js-mainnav" class="clearfix megamenu horizontal left blue noJS">
                <div id="css3-megaMenuToggle" class="megaMenuToggle">
                    Menu		<span class="megaMenuToggle-icon"></span>
                </div>
                <div class="js-megamenu clearfix" id="js-meganav">
                    <ul class="megamenu level0" id="primaryNav" clientidmode="Static" runat="server">
                        <%--Controls get injected here by the code behind--%>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
