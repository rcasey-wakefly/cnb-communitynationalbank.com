﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MenuLayouts.Models;

public partial class App_UserControls_UtilityNav : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            var menuModel = new UtilityNav();

            var columns = MenuService.GetColumns(menuModel).ToList();

            if(!columns.Any())
            {
                return;
            }

            if (columns[0].Children != null)
            {
                rptFirstColumn.DataSource = columns[0].Children;
                rptFirstColumn.DataBind();
            }


            if (columns.Count > 1 && columns[1].Children != null)
            {
                rptSecondColumn.DataSource = columns[1].Children;
                rptSecondColumn.DataBind();
            }
        }
    }

    public bool AddPipe(int itemIndex)
    {
        return itemIndex < rptFirstColumn.Items.Count;
    }
}