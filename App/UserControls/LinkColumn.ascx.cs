﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMSApp.App.UserControls
{
    public partial class LinkColumn : System.Web.UI.UserControl
    {
        public string NodeAliasPath
        {
            get { return nodeAliasPath; }
            set
            {
                nodeAliasPath = value + "/%";
                rptLinkColumns.Path = nodeAliasPath;
                rptLinkColumns.ReloadData(true);
            }
        }

        private string nodeAliasPath { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}