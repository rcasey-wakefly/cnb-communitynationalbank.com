﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.DocumentEngine;
using FiftyOne.Foundation.UI.Web;
using Lucene.Net.Documents;
using TreeNode = CMS.DocumentEngine.TreeNode;

namespace CMSApp.App.UserControls
{
	public partial class Breadcrumbs : System.Web.UI.UserControl
	{

		#region Properties

		public List<TreeNode> Pages = new List<TreeNode>();
		public bool HideBreadCrumbs { get; set; }

		#endregion
		
		#region Events

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HideBreadCrumbs)
			{
				if (DocumentContext.CurrentDocument == null)
				{
					Response.Redirect("/");
				}

				
				CreateHierarchy(this.Pages, DocumentContext.CurrentDocument);

				this.Pages.Reverse();
				repBreadCrumbs.DataSource = this.Pages;
				repBreadCrumbs.DataBind();
			}
		}

		protected void repBreadCrumbs_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e != null && e.Item != null && e.Item.DataItem is TreeNode)
			{
				var node = (TreeNode)e.Item.DataItem;

				HyperLink lnk = (HyperLink)e.Item.FindControl("LinkTarget");
				Literal lnkText = (Literal)e.Item.FindControl("LinkText");

				lnk.NavigateUrl = node.NodeAliasPath;

				if (e.Item.ItemIndex == Pages.Count - 1)
				{
					Literal containerOpen = (Literal)e.Item.FindControl("ContainerOpen");
					containerOpen.Text = "<span class='result-category'>";

					lnkText.Text = SetLinkText(node.DocumentName, true);
					
					Literal containerClose = (Literal)e.Item.FindControl("ContainerClose");
					containerClose.Text = "</span>";
				}
				else
				{
					lnkText.Text = SetLinkText(node.DocumentName, false);
				}
			}
		}

		#endregion

		#region Helpers

		private static string SetLinkText(string documentName,bool isLast)
		{
			return isLast ? documentName : String.Format("<span>{0}</span>", documentName);
		}

		private static void CreateHierarchy(List<TreeNode> pages, TreeNode page)
		{
			if (page == null)
			{
				return;
			}

			pages.Add(page);
			CreateHierarchy(pages, page.Parent);
		}

		#endregion

	}
}