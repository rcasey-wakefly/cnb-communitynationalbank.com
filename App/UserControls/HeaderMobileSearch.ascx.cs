﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.Helpers;

namespace CMSApp.App.UserControls
{
    public partial class HeaderMobileSearch : System.Web.UI.UserControl
    {
        public string CssClass { get; set; }
        public string Placeholder { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                mbtxtSearch.Attributes.Add("placeholder", Placeholder);
            }
        }

        protected void btnSearchMB_OnServerClick(object sender, EventArgs e)
        {
            string searchText = !string.IsNullOrEmpty(mbtxtSearch.Value) ? mbtxtSearch.Value : string.Empty;
            if (!String.IsNullOrEmpty(searchText))
            {
                Response.Redirect(URLHelper.ResolveUrl("~/Search-Results?SearchText=" + searchText));
            }
        }
    }
}