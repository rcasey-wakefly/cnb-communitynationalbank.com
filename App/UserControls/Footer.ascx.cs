﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMSApp.App.UserControls
{
    public partial class Footer : System.Web.UI.UserControl
    {
        public IEnumerable<MenuColumn> Columns { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                var menuModel = new MenuLayouts.Models.Footer();

                Columns = MenuService.GetColumns(menuModel);

                rptColumns.DataSource = Columns;
                rptColumns.DataBind();
            }
        }

        public string GetDisplayText(string displayText)
        {
            if(string.IsNullOrEmpty(displayText))
            {
                return string.Empty;
            }

            var displayTextArray = displayText.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            var firstWord = displayTextArray[0];

            var remainingText = string.Empty;

            if (displayTextArray.Length > 1)
            {
                for (int i = 1; i < displayTextArray.Length; i++)
                {
                    remainingText += " " + displayTextArray[i];
                }
            }

            var strBuilder = new StringBuilder();
            strBuilder.Append(string.Format("{0} ", firstWord));
            strBuilder.Append(string.Format("<span class='other'>{0}</span>", remainingText));

            return strBuilder.ToString();
        }
    }
}