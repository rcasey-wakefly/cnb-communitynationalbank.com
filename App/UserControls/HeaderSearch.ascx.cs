﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.Helpers;

namespace CMSApp.App.UserControls
{
    public partial class HeaderSearch : System.Web.UI.UserControl
    {
        public string CssClass { get; set; }
        public string Placeholder { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            txtSearch.Attributes.Add("placeholder", Placeholder);
        }

        protected void btnSearch_OnServerClick(object sender, EventArgs e)
        {
            string searchText = !string.IsNullOrEmpty(txtSearch.Value) ? txtSearch.Value : string.Empty;
            if (!String.IsNullOrEmpty(searchText))
            {
                Response.Redirect(URLHelper.ResolveUrl("~/Search-Results?SearchText=" + searchText));
            }
        }
    }
}