﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CalloutButton.ascx.cs" Inherits="CMSApp.App.UserControls.CalloutButton" %>

<cms:CMSRepeater runat="server" ID="rptCallouts" ClassNames="Common.CalloutButton">
    <ItemTemplate>
        <div class="col-4 col-12-medium col-12-small home-callout accordion-section">
            <div class="hc-heading">
	            <div class="hc-head-img">
		            <img src="<%#Eval("Image") %>" alt="<%#Eval("ImageAltText") %>">
	            </div>
	            <div class="hc-head-title">
		            <div class="hc-head-cell">
			            <h2 class="center-headline"><a href="<%#Eval("Link") %>"><%#Eval("HeaderText") %></a></h2>
		            </div>
	            </div>
            </div>
            <a class="accordion-toggle" href="#"><i class="fa fa-angle-down"></i></a>
            <div class="accordion-content">
	            <p><%#Eval("Description") %></p>
	            <a class="button blue-button" href="<%#Eval("Link") %>"><%#Eval("ButtonText") %></a>
            </div>
        </div>
    </ItemTemplate>
</cms:CMSRepeater>

