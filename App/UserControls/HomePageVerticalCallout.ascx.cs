﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMSApp.App.UserControls
{
    public partial class HomePageVerticalCallout : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string renderImage(string imgSrc, string alt)
        {
            if (!string.IsNullOrWhiteSpace(imgSrc))
            {
                return "<img src='" + imgSrc + "' alt='" + alt + "'>";
            }
            return "";
        }
    }
}