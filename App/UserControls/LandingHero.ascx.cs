﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.DocumentEngine;
using CMSApp.App.Classes.Helpers;

namespace CMSApp.App.UserControls
{
    public partial class LandingHero : BaseHero
    {
        public string HeroText { get; set; }   
        protected void Page_Load(object sender, EventArgs e)
        {
            innerHero.Style.Add("background", "url(" + ImagePath + ")" + BackgroundExtra);
            innerHero.Style.Add("background-size", Cover);
            HeroText = Hero.GetStringValue("HeroText", "");
        }
    }
}