﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FormDropDownList.ascx.cs" Inherits="App_UserControls_FormDropDownList" %>
<h3>Sign up or Apply <span class="other">for Services</span></h3>
<p>Select your service from the drop-down list below.</p>
<label for="ddlFormNames">Apply Form</label>
<asp:DropDownList runat="server" ID="ddlFormNames" ClientIDMode="Static" OnSelectedIndexChanged="ddlFormNames_OnSelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>