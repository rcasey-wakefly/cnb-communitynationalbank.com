﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HeaderMobileSearch.ascx.cs" Inherits="CMSApp.App.UserControls.HeaderMobileSearch" %>
<div class="search-block">
    <div class="search-row">
        <div class="search-field">
		    <label for="mbtxtSearch" class="visuallyhidden">Search</label>            
            <input type="search" placeholder="Search NH Historical Society" id="mbtxtSearch" runat="server" clientidmode="Static"/>
        </div>
        <div class="search-button">
            <button id="btnSearchMB" value="search" runat="server" clientidmode="Static" OnServerClick="btnSearchMB_OnServerClick">
                <i class="fa fa-search"></i><span class="visuallyhidden">Submit Search</span>
            </button>
        </div>
    </div>
</div>
