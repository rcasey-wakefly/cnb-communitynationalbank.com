﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.DocumentEngine;

[PartialCaching(120)]
public partial class App_UserControls_MegaMenu : System.Web.UI.UserControl
{
    public static int CacheMinutes { get { return GlobalSettingsHelper.GetSettingsKeyIntegerValue("MegaMenuCacheMinutes"); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        CachePolicy.Duration = TimeSpan.FromMinutes(CacheMinutes > 0 ? CacheMinutes : 60);

        MenuManager.CreateSiteMenu(primaryNav, Page, Request.UserAgent);
    }
}