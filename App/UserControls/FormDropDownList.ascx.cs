﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App.Helpers;
using CMS.Membership;
using Microsoft.Ajax.Utilities;
using org.pdfclown.util.collections.generic;

public partial class App_UserControls_FormDropDownList : System.Web.UI.UserControl
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var forms = CustomTableHelper.GetItems("customtable.formpages").OrderBy(x=>x.GetIntegerValue("ItemOrder",Int32.MaxValue)).Select(x => new ListItem() { Value = x.GetStringValue("FormLink", "/"), Text = x.GetStringValue("FormName", "Form Issue") }).ToList();
            forms.Insert(0, new ListItem(){Value = "0", Text = "Please Select"});
            ddlFormNames.Items.AddRange(forms.ToArray());
        }
    }

    protected void ddlFormNames_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlFormNames.SelectedValue != "0")
        {
            Response.Redirect(ddlFormNames.SelectedValue);
        }
    }
}