﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FormQuickLinkRepeater.ascx.cs" Inherits="App_UserControls_FormQuickLinkRepeater" %>

<div class="col lg-6 sm-12">
    <div class="col-inner cnb-home-quicklinks">
        <h1 class="cnb-header">Quick Links</h1>
        <div class="cnb-home-signup">
            <h1 class="cnb-home-signup-headline">
                Open a Deposit Account Today!
            </h1>
            <div>
                <a href="https://jha.loanspq.com/Consumer/login/default.aspx?enc2=CjDDGQgp6ViDEpMSQAmj1FZeQNrAC2W-Ei3l385mkVyMc7AMbcmJqDn1CfgfDiBjsHei5Ot0jZI_tGQKf8qljPw5tfhX-buJT7R1WsWheqPYyaD8VZkjHmXpJUNXriCY" class="cnb-button" target="_blank">Open Account</a>
            </div>
        </div>
        <ul class="cnb-links"> 
            <cms:CMSRepeater runat="server" ID="rptQuickLinks">
                <ItemTemplate>
                    <li>
                        <a href="<%# Eval("Value") %>"><%# Eval("Text") %></a>
                    </li>
                </ItemTemplate>
            </cms:CMSRepeater>
        </ul>
    </div>
</div>