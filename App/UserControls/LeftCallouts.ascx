﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LeftCallouts.ascx.cs" Inherits="CMSApp.App.UserControls.LeftCallouts" %>
<div class="<%=CssClass %>">
    <a href="<%#Eval("Link") %>"><img src="<%#Eval("Image") %>" alt="<%#Eval("ImageAltText") %>"></a>
    <span><a href="<%#Eval("Link") %>"><%#Eval("HeaderText") %></a></span>
</div>