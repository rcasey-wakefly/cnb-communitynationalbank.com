﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Amazon.DynamoDB.DocumentModel;
using CMS.DocumentEngine;
using CMSApp.App.Classes;
using CMSApp.App.Classes.Helpers;
using MenuLayouts.Helpers;
using TreeNode = CMS.DocumentEngine.TreeNode;

namespace CMSApp.App.UserControls
{
    public partial class LeftNav : System.Web.UI.UserControl
    {
        public TreeNode CurrentDocument { get; set; }
        public TreeNode TopLink { get; set; }
        public String TopLinkName { get; set; }
        public List<TreeNodeLink> FirstLevelLinks { get; set; }
        public IEnumerable<TreeNode> NodesList { get; set; }
        public IEnumerable<string> allPaths { get; set; }
        
        #region constants
        public const string UlOpen = "<ul>";
        public const string UlClose = "</ul>";
        public const string LiOpen = "<li class='sublevel'>";
        public const string LiClose = "</li>";
        public const string AOpen = "<a href='{0}' class='{1}'>";
        public const string AClose = "</a>";
        public const string SpanOpen = "<span>";
        public const string SpanClose = "</span>";
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            CurrentDocument = DocumentContext.CurrentDocument;
            if (CurrentDocument == null) return;

            NodesList = DocumentHelper.GetDocuments("ST.MenuItem")
                .Path("/", PathTypeEnum.Children)
                .OrderBy("NodeOrder")
                .WhereEquals("MenuItemGroup", "left")
                .WhereEquals("DocumentMenuItemHideInNavigation", 0)
                .NestingLevel(1)
                .Columns("NodeAliasPath, DocumentName, DocumentMenuCaption", Constants.MegaMenuLinkTypeField);
            FirstLevelLinks = new List<TreeNodeLink>();
           SetNodes();

        }

        private IEnumerable<string> GetAllChildrenPaths(TreeNodeLink currentLink)
        {
            var paths = new List<string>();

            foreach (var child in currentLink.Children)
            {
                paths.Add(child.NodeAliasPath);

                if (child.Children.Any())
                {
                    paths.AddRange(GetAllChildrenPaths(child));
                }
            }

            return paths;
        }

        public void SetNodes()
        {
            foreach (var node in NodesList)
            {
                var tnode = new TreeNodeLink
                {
                    DocumentName = node.DocumentName, 
                    Node = node, 
                    Children = new List<TreeNodeLink>(), 
                    NodeAliasPath = node.NodeAliasPath,
                    LinkType = (MenuLink.LinkTypes)node.GetIntegerValue(Constants.MegaMenuLinkTypeField, 0)
                };
                
                FirstLevelLinks.Add(tnode);
                addBrowserItems(tnode);
                tnode.ShowChildren = tnode.NodeAliasPath == CurrentDocument.NodeAliasPath || GetAllChildrenPaths(tnode).Contains(CurrentDocument.NodeAliasPath);
            }
        }

        private void addBrowserItems(TreeNodeLink parentNode)
        {
            //var children = DocumentHelper.GetDocuments().Types(NavClassNames).Path(parentNode.Node.NodeAliasPath, PathTypeEnum.Children).NestingLevel(1).WhereEquals("DocumentMenuItemHideInNavigation", 0);
            var children = DocumentHelper.GetDocuments("ST.MenuItem")
                .Path(parentNode.NodeAliasPath, PathTypeEnum.Children)
                .OrderBy("NodeOrder")
                .WhereEquals("MenuItemGroup", "left")
                .WhereEquals("DocumentMenuItemHideInNavigation", 0)
                .Columns("NodeAliasPath, DocumentName, DocumentMenuCaption", Constants.MegaMenuLinkTypeField)
                .NestingLevel(1);
                

            
            foreach (var child in children)
            {
                var cnode = new TreeNodeLink
                {
                    DocumentName = string.IsNullOrWhiteSpace(child.DocumentMenuCaption) ? child.DocumentName : child.DocumentMenuCaption, 
                    Node = child, 
                    Children = new List<TreeNodeLink>(), 
                    NodeAliasPath = child.NodeAliasPath,
                    LinkType = (MenuLink.LinkTypes)child.GetIntegerValue(Constants.MegaMenuLinkTypeField, 0)
                };
                if (cnode.NodeAliasPath == CurrentDocument.NodeAliasPath)
                {
                    parentNode.ShowChildren = true;
                    cnode.ShowChildren = true;
                }
                parentNode.Children.Add(cnode);
                //this is recursive but right now cnb only needs a very simple left nav - 2 levels
                addBrowserItems(cnode);
            }
        }

        public string DisplayTree(TreeNodeLink parentNode)
        {
            var sb = new StringBuilder();
            sb
                .Append(LiOpen);
            if (parentNode.LinkType != MenuLink.LinkTypes.Header)
            {
               //Link.ShowChildren ? "class='sublevel_current' id='active_menu'" : "class='sublevel'" %>
                sb.Append(String.Format(AOpen, parentNode.Node.NodeAliasPath, parentNode.ShowChildren?"sublevel_current":"sublevel"))
                    .Append(parentNode.DocumentName)
                    .Append(AClose);
            }
            else
            {
                sb.Append(SpanOpen)
                    .Append(parentNode.DocumentName)
                    .Append(SpanClose);
            }
            
            //this is recursive but right now cnb only needs a very simple left nav - 2 levels
            if (parentNode.Children.Any() && parentNode.ShowChildren)
            {
                sb.Append(UlOpen);
                foreach (var child in parentNode.Children)
                {
                    sb.Append(DisplayTree(child));
                }
                sb.Append(UlClose);
            }
            
                sb.Append(LiClose);
            
            return sb.ToString();
        }
    }
}