﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.UIControls;

namespace CMSApp.App.Master
{
    public partial class Base : TemplateMasterPage
    {
        private string _bodyClass;
        public string BodyClass
        {
            get { return _bodyClass; }
            set { _bodyClass = value; }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            this.ltlTags.Text = this.HeaderTags;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Check if the page that is loading is the home page. If it is, display the Alert (if there is one)
            alertViewer.Visible = CurrentPage.NodeAliasPath.Equals("/Home");

            var menu = LoadControl("~/App/UserControls/MegaMenu.ascx") as PartialCachingControl;

            if(menu != null)
            {
                plcMenu.Controls.Add(menu);
            }
        }
    }
}