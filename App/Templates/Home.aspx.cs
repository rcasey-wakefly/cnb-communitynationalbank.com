﻿using CMS.UIControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class App_Templates_Home : TemplatePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var masterPage = Master;
        if (masterPage != null)
        {
            masterPage.BodyClass += "site com_content view-featured no-layout no-task itemid-101";
        } 
    }
}