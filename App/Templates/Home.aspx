﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/MasterPages/InnerOneColumn.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="App_Templates_Home" %>
<%@Register src="~/App/UserControls/Hero.ascx" tagName="hero" tagPrefix="home" %>
<%@Register src="~/App/UserControls/ModularContent.ascx" tagName="Content" tagPrefix="Modular" %>
<%@register src="~/App/UserControls/AboveFooterContent.ascx" tagName="FooterContent" tagPrefix="Above" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ MasterType VirtualPath="~/App/MasterPages/InnerOneColumn.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headScripts" runat="Server">
    <%--<%: Scripts.Render("~/bundles/js/main") %>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="headStyles" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="article" runat="Server">
    <div class="row-fluid cnb-home-row-fluid">
        <home:hero runat="server"/>
        <!-- modular content-->
        <Modular:Content runat="server"/>
        <%--<Above:FooterContent runat="server"/>--%>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="bottomScripts" runat="Server">
</asp:Content>

