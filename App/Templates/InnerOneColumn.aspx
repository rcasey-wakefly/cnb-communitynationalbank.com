﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/MasterPages/InnerOneColumn.master" AutoEventWireup="true" CodeFile="InnerOneColumn.aspx.cs" Inherits="App_Templates_InnerOneColumn" %>
<%@ MasterType VirtualPath="~/App/MasterPages/InnerOneColumn.Master" %>
<%@register src="~/App/UserControls/AboveFooterContent.ascx" tagName="FooterContent" tagPrefix="Above" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headScripts" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="headStyles" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="article" Runat="Server">
    <cms:CMSPagePlaceholder runat="server">
		<LayoutTemplate>
            <cms:CMSWebPartZone ID="PageContent" runat="server" />
        </LayoutTemplate>
	</cms:CMSPagePlaceholder>
    <Above:FooterContent runat="server"/>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="bottomScripts" Runat="Server">
</asp:Content>

