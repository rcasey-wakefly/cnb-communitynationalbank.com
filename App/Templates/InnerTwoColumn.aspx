﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/MasterPages/InnerTwoColumn.master" AutoEventWireup="true" CodeFile="InnerTwoColumn.aspx.cs" Inherits="App_Templates_InnerTwoColumn" %>
<%@ MasterType VirtualPath="~/App/MasterPages/InnerTwoColumn.Master" %>
<%@Register src="~/App/UserControls/FormDropDownList.ascx" tagName="dropdown" tagPrefix="form" %>
<%@Register src="~/App/UserControls/LeftNav.ascx" tagName="nav" tagPrefix="left" %>
<%@Register src="~/App/UserControls/MegaMenu/MobileMenu.ascx" tagName="Menu" tagPrefix="Mobile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headScripts" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="headStyles" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="aside" Runat="Server">
    <%--Left side page content goes here--%>
    <div class="moduletable mobilejam">
        <Mobile:Menu ID="mobileMenu" runat="server"/>
    </div>
    <cms:CMSPagePlaceholder runat="server">
		<LayoutTemplate>
            <cms:CMSWebPartZone ID="AboveLeftNav" runat="server" />
        </LayoutTemplate>
	</cms:CMSPagePlaceholder>
    <div class="moduletable desktopjam">
        <left:nav runat="server"/>
    </div>
    <div class="moduletable internalapply">
        <form:dropdown runat="server"></form:dropdown>
    </div>
    <cms:CMSPagePlaceholder runat="server">
		<LayoutTemplate>
            <cms:CMSWebPartZone ID="BelowLeftNav" runat="server" />
        </LayoutTemplate>
	</cms:CMSPagePlaceholder>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="pageArticle" Runat="Server">
    <%--Main page content goes here--%>
    <cms:CMSPagePlaceholder runat="server">
		<LayoutTemplate>
            <cms:CMSWebPartZone ID="MainPageContent" runat="server" />
        </LayoutTemplate>
	</cms:CMSPagePlaceholder>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="bottomScripts" Runat="Server">
</asp:Content>

